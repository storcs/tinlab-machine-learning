package ann.math.function;

/**
 * Abstracte klasse om activation function makkelijk uit te kunnen wisselen
 */	
public abstract class ActivationFunction {
	/**
	 * De activation functie die per element wordt aangeroepen.
	 */
	public abstract double f(double z);

	/**
	 * De afgeleide van deze activation functie
	 */
	public abstract double fDeriv(double z);
}