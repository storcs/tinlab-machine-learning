package ann.math.function;

/**
 * Implementatie van de tanh functie
 */
public class HyperbolicTangent extends ActivationFunction {
	/**
	 * Een look-up table zou sneller kunnen zijn, maar vooralsnog voldoet het telkens berekenen.
	 */
	public double f(double z) {
		return Math.tanh(z);
	}

	public double fDeriv(double z) {
		return 1 / ( Math.pow( Math.cosh(z), 2 ) );
	}
}