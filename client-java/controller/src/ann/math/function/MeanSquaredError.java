package ann.math.function;

 public class MeanSquaredError extends ErrorFunction {
 	public double getError(double actualOutput, double desiredOutput) {
 		return 0.5 * Math.pow(desiredOutput - actualOutput, 2.0);
 	}

 	/**
 	 * actualOutput = activation = a_i
	 * desiredOutput = t_i
	 */
 	public double errorDerivToActivation(double actualOutput, double desiredOutput) {
 		//return 0.5*Math.pow(desiredOutput, 2) - desiredOutput + actualOutput;
 		return -(desiredOutput - actualOutput);
 	}
 }