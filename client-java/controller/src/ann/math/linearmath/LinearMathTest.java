package ann.math.linearmath;

/**
 * Klasse om LinearMath klasse mee te testen.
 * Ondertussen niet meer nodig.
 */
public class LinearMathTest {
	public static void main(String[] args) {
		double[][] v = {{5.0,	4.0,	1.0,	2.0,	3.0}};
		double[][] w = {{4.0},
						{6.0},
						{7.0},
						{-8.0},
						{-9.0}};

		double[][] a = {{1.0,	2.0},
						{4.0,	3.0}};
		double[][] b = {{1.0,	2.0,	3.0},
						{3.0,	-4.0,	7.0}};


		System.out.println("v=");
		LinearMath.print(v);
		System.out.println("\nw=");
		LinearMath.print(w);

		System.out.println("\nvt=");
		LinearMath.print(LinearMath.transpose(v));
		System.out.println("\nwt=");
		LinearMath.print(LinearMath.transpose(w));

		System.out.println("\nv + wt=");
		LinearMath.print( LinearMath.add(v,LinearMath.transpose(w)) );


		System.out.println("\nA=");
		LinearMath.print(a);
		System.out.println("\nB=");
		LinearMath.print(b);

		System.out.println("\nAt=");
		LinearMath.print(LinearMath.transpose(a));
		System.out.println("\nBt=");
		LinearMath.print(LinearMath.transpose(b));

		System.out.println("\nAxB=");
		LinearMath.print( LinearMath.multiply(a, b) );		
	}
}