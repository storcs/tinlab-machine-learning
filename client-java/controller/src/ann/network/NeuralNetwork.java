package ann.network;

import ann.math.linearmath.LinearMath;
import ann.math.function.*;
import ann.util.io.CSVReader; //Nodig voor trainen en voor printen

public class NeuralNetwork {
	/**
	 * De verschillende lagen van het neurale netwerk. <i>Exclusief</i> inputs, maar
	 * <i>inclusief</i> outputs, dus: layers[0] is de eerste laag na de inputs en
	 * layers[layers.length-1] is de laag van de outputs.
	 * De gewichtsmatrix van layers[i] is de matrix van laag i-1 naar laag i toe.
	 */
	private NeuronLayer[] layers;


	/**
	 * Een array met daarin alle sommen (dus de z in a=f(z)) uit de laatste aanroep van NeuralNetwork#getOutputs().
	 * Wordt bijgehouden, zodat deze in de updateWeights niet nogmaals berekend hoeven worden.
	 * Aanspreken van sommen is: inputSum[l][i] waar l is de laag en i is het nummer van de node in die laag.
	 */
	private double[][] inputSum;

	/**
	 * Een array met daarin alle inputs uit de laatste aanroep van NeuralNetwork#getOutputs().
	 * Wordt bijgehouden, zodat deze in NeuralNetwork#updateWeights() gebruikt kunnen worden
	 * om de Error te berekenen
	 */
	private double[] networkInputs = null;

	/**
	 * De functie waarmee de error berekend wordt
	 */
	private ErrorFunction errorFunction;

	/**
	 * Constructor maakt het object, maar initialiseert nog niet de lagen.
	 * @param nrOfLayers	Het aantal lagen van het neurale netwerk. <i>Exclusief</i> inputs,
	 *						maar <i>inclusief</i> outputs.
	 */
	public NeuralNetwork(int nrOfLayers, ErrorFunction errorFunction) {
		layers = new NeuronLayer[nrOfLayers];
		//connections = new LayerConnection[nrOfLayers];
		inputSum = new double[nrOfLayers][];
		this.errorFunction = errorFunction;
	}

	public double[] getOutputs(double[] in) {
		//Sla inputs op
		for(int i = 0; i < in.length; i++) {
			networkInputs[i] = in[i];
		}

		// Inputs staan in een array (noem het een vector), maar om het te 
		// vermenigvulden met multiply() moet het een matrix zijn. Maak er
		// daarom een 1xN matrix van. Ook moet het een kolomvector zijn en
		// niet een rijvector, dus transponeer:
		double[][] a = {in};
		a = LinearMath.transpose(a);

		

		for(int i = 0; i < layers.length; i++) {
			// W is een Mx(N+1) matrix en a is een vector met N elementen.
			// W is de matrix met alle gewichten van de signalen op de waarden van de volgende laag.
			// a is de vector met alle waarden van de signalen uit de vorige laag met lengte N
			// Er moet nog een 1 als laatste element van a worden toegevoegd voor de bias
			double[][] noBias = a;
			a = new double[noBias.length+1][1];
			for(int j = 0; j < noBias.length; j++) {
				a[j][0] = noBias[j][0];
			}
			a[noBias.length][0] = 1.0; //Voegt bias toe

			//Het resultaat wordt ook weer geschreven naar a, omdat dit weer als input gebruikt wordt bij het berekenen van de volgende laag.
			a = LinearMath.multiply( layers[i].weight, a );

			//Sla de sommen op, zodat ze gebruikt kunnen worden bij het update van de weights zonder dat ze opneiuw
			//berekend hoeven worden:
			for(int j = 0; j < a.length; j++) {
				inputSum[i][j] = a[j][0];
			}


			//a is nu de som van de inputs + de bias
			//Nu moet elke waarde z in de vector a vervangen worden door f(z).
			for(int j = 0; j < a.length; j++) { //a is een kolomvector, dus rij is index, kolom is altijd eerste element (0)
				a[j][0] = layers[i].f[j].f( a[j][0] );	//Pak de activatie functie van deze neuron en geef de inputsom er aan mee
			}

			//a is nu een kolomvector met de waarden van de volgende laag van het netwerk. Hiermee kunnen dezelfde stappen
			//herhaald worden voor een volgende laag, of deze waarden zijn de outputs als dit de laatste laag is.
		}


		//Transponeer a, zodat alle outputs in een rijvector komen in plaats van een kolomvector:
		a = LinearMath.transpose( a );

		//De bovenste rij zijn nu de outputs. Return daarom deze bovenste rij:
		return a[0];
	}

	/**
	 * Update de gewichten.
	 * @return De error van het netwerk
	 */
	public double updateWeights(double[] actualOutputs, double[] desiredOutputs, double learningRate) {
		double[] previousGradient_dEda = null; //Kolomvector, waarin de gradienten (=partiele afgeleide van Error naar activation) van laag i+1 in staan

		//Begin bij achterste laag en propageer van achter naar voren door netwerk heen:
		for(int l = layers.length-1; l >= 0; l--) {

			//
			double[] gradient_dEda = null;

			//========== dE/da_i ==========
			//Bereken gradient = de partiele afgeleide van error naar activation
			//dE/da
			if(l == layers.length-1) {	//Als dit de output laag is
				gradient_dEda = new double[ layers[l].weight.length ]; //Ruimte voor alle nodes in de outputlaag

				for(int i = 0; i < layers[l].weight.length; i++) { //Voor elke node i in deze laag l
					//dE/da is de partiële afgeleide van Error naar activation van i (=a_i)
					//a_i = output i van het netwerk
					gradient_dEda[i] = errorFunction.errorDerivToActivation( actualOutputs[i], desiredOutputs[i] );
				}

			} else { //Voor hidden lagen:
				// dE/a_i = som[n=0 tot k]( w_k,i * f'(in_k) * dE/da_k )
				// N_k zijn alle nodes waarvoor N_i als input kan dienen, dus k = layers[l+1].weight.length
				// w_k,i staat in de matrix van laag L naar L+1, dus w_k,j = layers[l+1].weight[k][i]
				// in_k staat in inputSum, dus f'(in_k) = layers[l+1].f.fDeriv( inputSum[l+1][k] )
				// dE/da_k staat in previousGradient_dEda array, dus: dE/da_k = previousGradient_dEda[k]
				
				//De waarde die bepaald wordt is dE/da_i, dus dE/da_i = gradient[i]

				gradient_dEda = new double[ layers[l].weight.length ]; //Ruimte voor alle nodes in deze laag

				for(int i = 0; i < layers[l].weight.length; i++) { //Voor elke node i in deze laag l
					double dErrSum = 0.0; //Houdt de som in bij

					for(int k = 0; k < layers[l+1].weight.length; k++) { //Voor elke node k in laag l+1 (maar length-1, want i kan geen invloed uitoefenen op bias)
						double w_ki = layers[l+1].weight[k][i];
						double fDeriv_in_k = layers[l+1].f[k].fDeriv( inputSum[l+1][k] );

						dErrSum += w_ki * fDeriv_in_k * previousGradient_dEda[k];

						
					}

					gradient_dEda[i] = dErrSum; //Partiele afgeleide van dError naar dActivation_i is de som van dError van alle nodes waarvoor i als input kan dienen
				}
				
			}

			//Gradient bevat nu de partiele afgeleide dE/da_i


			//========== Bewaar gradienten ==========
			//Stel gradient dE/da in voor volgende iteratie
			//Wordt nu bewaard, omdat dE/da hierna vergeten wordt
			previousGradient_dEda = gradient_dEda;


			
			//Een matrix met daarin de gradienten die bij de gewichtsmatrix opgeteld moeten worden
			//Matrix met gradienten moet opgeteld bij weight matrix, dus even groot zijn.
			double[][] gradient = new double[ layers[l].weight.length ][ layers[l].weight[0].length ];


			//========== dE/dw_i,j ==========
			for(int i = 0; i < layers[l].weight.length; i++) { //i is de index van node in huidige laag L
				for(int j = 0; j < layers[l].weight[0].length; j++ ) { //j is de index van de node uit laag L-1
					// De activation a_j van Node j = f( inputSum[l-1][j] ), want gewoon invullen van in_i in f()
					// f'(in_i) = layers[l].f'( inputSum[l][i] )
					// dE/da_i = gradient_dEda[i]

					// Activation van Node j uit laag L-1 is f(z) met:
					// z = som(w*a) + bias
					double a_j = 1.0; //Als j is laatste node, dan is de node de bias, dus is de activation altijd 1.0
					if(j != layers[l].weight[0].length-1) { //Als j niet de laatste node, bereken dan activation
						if(l==0) { // Als L=0, dan is L-1 de inputs
							a_j = networkInputs[j];
						} else {
							a_j = layers[l-1].f[j].f( inputSum[l-1][j] ); //Roep f voor activation van node j
						}
					}
						
					

					// f'(in_i) = afgeleide van f(z) uit de laag van a_i
					// in_i uit de vorige iteratie staat in de inputSum matrix
					double f_in_i = layers[l].f[i].fDeriv( inputSum[l][i] );

					// dE/da_i staat al op plek [i][j] in de gradient matrix, dus neem waarde en overschrijf
					// gradient = a_j * f`(in_i) * dE/da_i;
					gradient[i][j] = gradient_dEda[i] * a_j * f_in_i;
				}				
			}

			//gradient bevat nu de partiele afgeleide van error naar het gewicht van Node j uit laag L-1 op Node i uit laag L
			//gradient[i][j] = dE/dw_i,j

			//========== Update de gewichtsmatrix ==========
			//Vermenigvuldig gradienten met de learning rate en trek
			//alle gradienten van de gewichtsmatrix af: (dus -eta*gradient)
			layers[l].weight = LinearMath.add( layers[l].weight, LinearMath.multiply( -learningRate, gradient) );

		}


		//Bereken totale error en return
		double error = 0.0;
		for(int i = 0; i < actualOutputs.length; i++ ) {
			error += Math.abs(this.errorFunction.getError(actualOutputs[i], desiredOutputs[i]));
			//System.out.println("actualOutput: "+actualOutputs[i]+"\tdesiredOutput: "+desiredOutputs[i]+"\terror: "+Math.abs(this.errorFunction.getError(actualOutputs[i], desiredOutputs[i])));
		}
		return error;

	}




	public void setLayer(int index, NeuronLayer layer) {
		if(index < 0 || index >=layers.length) {
			throw new RuntimeException("Invalid index: "+index);
		}
		layers[index] = layer;
		inputSum[index] = new double[layers[index].weight.length];

		if(index == 0) { //Als dit van laag 0 naar 1 (input naar hidden laag 1), dan is nu het aantal inputs bekend:
			networkInputs = new double[ layers[index].weight[0].length-1 ]; //Doe -1 om niet de bias mee te nemen
		}
	}

	public NeuronLayer[] getLayers() {
		return layers;
	}

	public int getNrOfLayers() {
		return layers.length;
	}

	/**
	 * Controleert:
	 * - null-checks (op layers en connections, maar ook op elementen daarvan)
	 * - Zijn er biases voor elke layer.
	 * - Heeft elke layer een f(z).
	 * - Zijn er weights naar elke layer toe.
	 * - Zijn de arrays met biases de juiste lengte.
	 * - Zijn alle rijen van matrices even lang.
	 * - Komen de dimensies van de matrices overeen met het aantal biases.
	 *
	 * Of het aantal inputs of outputs juist is wordt <i>niet</i> gecontroleerd.
	 */
	public boolean isComplete(){

		if(layers == null || layers.length == 0) { //Controleer dat array uberhaupt is geïnstantieerd
			System.out.println("'layers' array was not instantiated.");
			return false;
		}

		for(int i = 0; i < layers.length; i++) { //Controle dat alle elementen van de arrays goed geïnstanteerd zijn
			//Controleer op nullptrs
			if(layers[i] == null) {
				System.out.println("Element "+i+" of 'layers' was not instantiated.");
				return false;
			}

			//Controleer op nullptrs
			if(layers[i].f == null ) {
				System.out.println("f from layer["+i+"] was not instantiated.");
				return false;
			}
			if(layers[i].weight == null) {
				System.out.println("Weight of layer["+i+"] was not instantiated.");
				return false;
			}
			if(layers[i].weight.length == 0) {
				System.out.println("Length of weight of layer["+i+"] is 0.");
				return false;
			}
			if(layers[i].path==null) {
				System.out.println("Path of layer["+i+"] was not instantiated.");
				return false;
			}
			
			//Controleer dat alle rijen in de matrix even lang zijn:
			for(int j = 1; j < layers[i].weight.length; j++) { //Begin bij 1, want controleren vanaf sizeof(j-1)==sizeof(j), maar index moet 0 zijn
				if(layers[i].weight[j-1].length != layers[i].weight[j].length) {
					System.out.println("Not all rows in the weight matrix "+j+" are of the same length." );
					return false;
				}
			}
			
			for(int j = 0; j < layers[i].f.length; j++) {
				if(layers[i].f[j] == null) {
					System.out.println("Missing an activation fuction for neuron "+j+" in layer "+i+".");
					return false;
				}
				if(layers[i].f.length != layers[i].weight.length) {
					System.out.println("The amount of activation functions ("+layers[i].f.length+") does not match the amount of weights ("+layers[i].weight.length+").");
					return false;
				}
			}

			//Het aantal neuronen in deze laag moet gelijk zijn aan het aantal kolommen van de volgende
			//weight matrix, omdat er anders geen matrix vermenigvuldiging kan worden uitgevoerd.
			if(i<layers.length-1) { //-1 om IndexOutOfRange te voorkomen, omdat naar i+1 gecontroleerd wordt.
				
				//layers[i].weight[0].length = (aantal neuronen + bias) in laag L, dus doe length-1 om zonder bias te krijgen
				//layers[i-1].weight.length = lengte van vector die uit laag L-1 komt. Deze moet even lang zijn als er neuronen in de volgende laag zijn
				if(layers[i].weight.length != layers[i+1].weight[0].length-1) {
					System.out.println("The weight matrix to layer "+(i+1)+" does not have "+
										"the correct dimensions ("+layers[i+1].weight.length+
										","+layers[i+1].weight[0].length+"-1) for the activation "+
										"vector from layer "+(i)+" ("+layers[i].weight.length+").");
					return false;
				}
			}
		}

		//Als hier aangekomen, dan geen probleem gevonden, dus return true
		return true;
	}

	/**
	 * Print het aantal lagen
	 */
	public void printSize() {
		System.out.println(layers.length + " layers.");
		for(int i = 0; i < layers.length; i++) {
			System.out.println(i+":\tWeight RxC="+layers[i].weight.length+"x"+layers[i].weight[0].length+"\tNeurons="+layers[i].f.length);
		}
		
	}

	/**
	 * Print de gewichten van de verbindingen tussen alle lagen
	 */
	public void printLayers() {
		System.out.println("Weights:");
		for(int i = 0; i < layers.length; i++) {
			System.out.print("-->"+i+":");

			for(int j = 0; j < layers[i].f.length; j++) {
				if( layers[i].f[j] instanceof Linear ) {
					System.out.print("\t"+CSVReader.LINEAR+",");
				} else if( layers[i].f[j] instanceof Sigmoid ) {
					System.out.print("\t"+CSVReader.SIGMOID+",");
				} else if( layers[i].f[j] instanceof HyperbolicTangent ) {
					System.out.print("\t"+CSVReader.TANH+",");
				} else if( layers[i].f[j] instanceof Softmax ) {
					System.out.print("\t"+CSVReader.SOFTMAX+",");
				} else {
					System.out.print("\tunknown,");
				}
			}
			System.out.println();
				

			for(int r = 0; r < layers[i].weight.length; r++) {
				for(int c = 0; c < layers[i].weight[r].length; c++) {
					System.out.print("\t"+layers[i].weight[r][c]);
				}
				System.out.println();
				
			}
			System.out.println("\t("+layers[i].path+")");
		}
	}

	public void printInputSums() {
		for(int i = 0; i < inputSum.length; i++) {
			System.out.print("layer "+i+": ");
			for(int j = 0; j < inputSum[i].length; j++) {
				System.out.print("\t"+inputSum[i][j]+",");
			}
			System.out.println();
		}
	}
}