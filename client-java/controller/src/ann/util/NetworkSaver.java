package ann.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import ann.network.NeuralNetwork;
import ann.network.NeuronLayer;
import ann.math.function.Sigmoid;
import ann.math.function.Linear;
import ann.math.function.HyperbolicTangent;
import ann.math.function.Softmax;
import ann.util.io.CSVReader;


/**
 * Klasse om neurale netwerken op te slaan naar configuratiebestanden
 */
public class NetworkSaver {
	private final static String layerPath = "layers/"; //Subdirectory waarnaar de lagen geschreven worden.
	private final static String layerName = "layer"; //Prefix voor bestandsnaam voor een laag.
	private final static String textFileName = "files.txt"; //Bestandsnaam voor configuratie

	/**
	 * Deze functie slaat een netwerk op naar de meegegeven directory.
	 * Het bestand "files.txt" bevat de namen van de verschillende bestanden
	 * die aangemaakt zijn.
	 * Voor elke laag van het netwerk wordt een .csv bestand gemaakt met de
	 * naam layerX.csv, waarbij X het nummer van de laag is.
	 */
	public static void saveNetwork(NeuralNetwork network, String outputDirectory) throws Exception {
		System.out.println("Saving network");

		File saveDirectory = new File(outputDirectory); 

		if(!saveDirectory.isDirectory()) { //Make de directory als hij nog niet bestaat
			System.out.println("Creating new directory for network: '"+outputDirectory+"'.");
			saveDirectory.mkdir();
		}

		//Text bestand met daarin alle namen van de layer bestanden
		File textFile = new File(outputDirectory+textFileName);
		if(textFile.delete()) { System.out.println("Deleted existing file '"+outputDirectory+textFileName+"'."); }

	    java.io.PrintWriter textOut = new java.io.PrintWriter(textFile); //Open het textbestand
	    textOut.println("#Number of layers in the network:");
	    textOut.println( Integer.toString(network.getNrOfLayers()) );
	    textOut.println("#List of .csv files containing the weight matrices:");


		File layerDirectory = new File(outputDirectory+layerPath);
		if(!layerDirectory.isDirectory()) { //Make de directory als hij nog niet bestaat
			System.out.println("Creating new directory for layer csv files: '"+outputDirectory+layerPath+"'.");
			layerDirectory.mkdir();
		}

		NeuronLayer[] layers = network.getLayers();
		for(int l = 0; l < layers.length; l++) {

			//========== Bestanden openen ==========
			String csvFilename = outputDirectory+layerPath+layerName+l+".csv";

			File layerCSV = new File(csvFilename); //Het bestand met daarin de matrix met alle gewichten


			//Verwijder de bestanden als ze al bestaan
			if(layerCSV.delete()) { System.out.println("Deleted existing file '"+csvFilename+"'"); }

	        java.io.PrintWriter csvOut = new java.io.PrintWriter(layerCSV); //Open bestand voor de gewichten

	        //Zet eerst huidige tijd in bestand
	        csvOut.println("#"+ new SimpleDateFormat("dd MMM, yyyy HH:mm:ss").format(Calendar.getInstance().getTime()) ); //Code van: https:/stackoverflow.com/questions/5175728/how-to-get-the-current-date-time-in-java



			//========== Activatie functies ==========
	        csvOut.println("#Activation functions:");
			StringBuilder str = new StringBuilder("");
			for(int i = 0; i < layers[l].f.length; i++) {

				if( layers[l].f[i] instanceof Linear ) {
					str.append(CSVReader.LINEAR);
				} else if( layers[l].f[i] instanceof Sigmoid ) {
					str.append(CSVReader.SIGMOID);
				} else if( layers[l].f[i] instanceof HyperbolicTangent ) {
					str.append(CSVReader.TANH);
				} else if( layers[l].f[i] instanceof Softmax ) {
					str.append(CSVReader.SOFTMAX);
				} else {
					throw new Exception("Could not determine the implementation of ActivationFunction for neuron "+i+" in layer "+l+".");
				}
				str.append( "," );
			}
			if(str.charAt( str.length()-1 ) == ',') { //Controleer dat laatste karakter inderdaad comma is
				str.deleteCharAt(str.length()-1); //Verwijder laatste comma
			}
			csvOut.println( str.toString() ); //Schrijf hele rij naar een regel




	        //========== Gewichten ==========

			//Schrijf de weight matrix naar de csv
			csvOut.println("#Weight matrix:");
			for(int i = 0; i < layers[l].weight.length; i++) {
				str = new StringBuilder("");

				for(int j = 0; j < layers[l].weight[i].length; j++) { //Voeg elementen voor deze rij een voor een toe
					str.append( Double.toString( layers[l].weight[i][j] )+"," );
				}

				if(str.charAt( str.length()-1 ) == ',') { //Controleer dat laatste karakter inderdaad comma is
					str.deleteCharAt(str.length()-1); //Verwijder laatste comma
				}

				csvOut.println( str.toString() ); //Schrijf hele rij naar een regel
			}


			//========== Bestanden sluiten ==========

			csvOut.close(); //Niet vergeten te sluiten
			System.out.println("Written layer to '"+csvFilename+"'");

			//Schrijf het csv bestand ook naar het textbestand, zodat het teruggevonden wordt
			textOut.println("l"+l+":"+layerPath+layerName+l+".csv");
		}


		textOut.close();
		System.out.println("Written configuration to '"+outputDirectory+textFileName+"'");

		System.out.println("Saved network to '"+outputDirectory+"'");
	}
}