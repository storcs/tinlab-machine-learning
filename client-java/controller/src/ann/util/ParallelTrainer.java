package ann.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalTime;

import ann.network.NeuralNetwork;


/**
 * Klasse om meerdere verschillende netwerken in verschillende thread te draaien.
 */
public class ParallelTrainer extends AbstractTrainer implements Runnable {

	
	//Maak een thread voor elke logische kern. Gebruik deze klasse
	//alleen als bestand op een SSD staat. Gebruik in andere gevallen
	//de klasse NetworkTrainer
	public final static int maxNrOfThreads = Runtime.getRuntime().availableProcessors();


	public static void main(String[] args) {

		LocalTime startTime = LocalTime.now();
		ArrayList<ParallelTrainer> trainers = new ArrayList<ParallelTrainer>();

/*
		String[] files = {	"alpine_2laps_total4.18_best2.06_0dmg.csv",
							"alpine_2laps_total4.21_best2.07_212dmg.csv",
							"alpine_2laps_total4.25_best2.09_231dmg.csv",
							"alpine_2laps_total4.30_best2.11-47dmg.csv" };
*/
/*
		String[] files = {	"CG_Speedway_1_3laps_total2.08_best0.40.csv",
							"CG_track_2_2laps_total1.55_best0.54.csv",
							"CG_track_2_3laps_total2.46_best0.52.csv",
							"CG_track_3_3laps_total3.17_best1.03.csv" };
*/
/*
		String[] files = {	"aalborg_2laps_total2.35_best1.14.csv",
							"alpine_2laps_total4.18_best2.06_0dmg.csv",
							"CG_Speedway_1_3laps_total2.08_best0.40.csv",
							"CG_track_2_3laps_total2.46_best0.52.csv",
							"CG_track_3_3laps_total3.17_best1.03.csv",
							"Ruudskogen_total2.17_best1.06.csv",
							"Street_1_total2.47_best1.20.csv",
							"Brondehach_3laps_total4.09_best1.21.csv",
							"Wheel_1_3laps_total4.31_best1.28.csv",
							"Wheel_2_3laps_total5.47_best1.51.csv" };
*/
		String[] files;

		String[] directories = {"configuration/size-test/1-hidden-layers_31-sigmoid_tanh-output_xavier-init/",
								"configuration/size-test/1-hidden-layers_31-sigmoid_xavier-init/",
								"configuration/size-test/1-hidden-layers_31-tanh_tanh-output_xavier-init/",
								"configuration/size-test/1-hidden-layers_31-tanh_xavier-init/",
								"configuration/size-test/1-hidden-layer_25-neurons_sigmoid_xavier-init/",
								"configuration/size-test/1-hidden-layer_25-neurons_tanh_random-init/",
								"configuration/size-test/1-hidden-layer_25-neurons_tanh_xavier-init/",
								"configuration/size-test/2-hidden-layers_27-sigmoid_25-sigmoid_xavier-init/",
								"configuration/size-test/2-hidden-layers_27-tanh_25-tanh_xavier-init/",
								"configuration/size-test/2-hidden-layers_31-sigmoid_31-sigmoid_tanh-output_xavier-init/",
								"configuration/size-test/2-hidden-layers_31-sigmoid_31-sigmoid_tanh-steering_xavier-init/",
								"configuration/size-test/2-hidden-layers_31-sigmoid_31-sigmoid_xavier-init/",
								"configuration/size-test/2-hidden-layers_31-tanh_31-tanh_tanh-output_xavier-init/",
								"configuration/size-test/2-hidden-layers_31-tanh_31-tanh_tanh-steering_xavier-init/",
								"configuration/size-test/2-hidden-layers_31-tanh_31-tanh_xavier-init/",
								"configuration/size-test/1-hidden-layers_31-sigmoid_linear-output_xavier-init/" };

		for(String directory : directories) {
			 NeuralNetwork net = NeuralNetworkBuilder.loadNetwork(directory+"untrained/", "files.txt");
		}

		

		for(String directory : directories) {

			files = new String[]{	"CG_Speedway_1_3laps_total2.08_best0.40.csv",
							"CG_track_2_2laps_total1.55_best0.54.csv",
							"CG_track_2_3laps_total2.46_best0.52.csv",
							"CG_track_3_3laps_total3.17_best1.03.csv" };

			trainers.add( new ParallelTrainer(	directory+"untrained/", //Begin configuratie
												directory+"trained-CG-tracks_x1_rate-0.05/", //Eind configuratie
												"../train_data/normalised/", //CSV locatie
												files, //Array met CSVs
												1, //Hoeveel keer door alle bestanden
												0.05, //Learning rate
												0.1) ); //Target error


			trainers.add( new ParallelTrainer(	directory+"untrained/", //Begin configuratie
												directory+"trained-CG-tracks_x5_rate-0.05/", //Eind configuratie
												"../train_data/normalised/", //CSV locatie
												files, //Array met CSVs
												5, //Hoeveel keer door alle bestanden
												0.05, //Learning rate
												0.1) ); //Target error

			trainers.add( new ParallelTrainer(	directory+"untrained/", //Begin configuratie
												directory+"trained-CG-tracks_x10_rate-0.05/", //Eind configuratie
												"../train_data/normalised/", //CSV locatie
												files, //Array met CSVs
												10, //Hoeveel keer door alle bestanden
												0.05, //Learning rate
												0.1) ); //Target error

			trainers.add( new ParallelTrainer(	directory+"untrained/", //Begin configuratie
												directory+"trained-CG-tracks_x50_rate-0.05/", //Eind configuratie
												"../train_data/normalised/", //CSV locatie
												files, //Array met CSVs
												50, //Hoeveel keer door alle bestanden
												0.05, //Learning rate
												0.1) ); //Target error

			trainers.add( new ParallelTrainer(	directory+"untrained/", //Begin configuratie
												directory+"trained-CG-tracks_x100_rate-0.05/", //Eind configuratie
												"../train_data/normalised/", //CSV locatie
												files, //Array met CSVs
												100, //Hoeveel keer door alle bestanden
												0.05, //Learning rate
												0.1) ); //Target error

			trainers.add( new ParallelTrainer(	directory+"untrained/", //Begin configuratie
												directory+"trained-CG-tracks_x500_rate-0.05/", //Eind configuratie
												"../train_data/normalised/", //CSV locatie
												files, //Array met CSVs
												500, //Hoeveel keer door alle bestanden
												0.05, //Learning rate
												0.1) ); //Target error

			trainers.add( new ParallelTrainer(	directory+"untrained/", //Begin configuratie
												directory+"trained-CG-tracks_x1000_rate-0.05/", //Eind configuratie
												"../train_data/normalised/", //CSV locatie
												files, //Array met CSVs
												1000, //Hoeveel keer door alle bestanden
												0.05, //Learning rate
												0.1) ); //Target error

			

		}


		Collections.sort(trainers, new Comparator<AbstractTrainer>() { //https://stackoverflow.com/questions/10396970/sort-a-list-that-contains-a-custom-class
		    public int compare(AbstractTrainer left, AbstractTrainer right)  {
		        //return left.getNrOfIterations() - right.getNrOfIterations();
			return right.getNrOfIterations() - left.getNrOfIterations();
		    }
		});

/*
		System.out.println(trainers.size()+" threads planned:");	
		for(AbstractTrainer nextTrainer : trainers) {
			System.out.print("\t");
			nextTrainer.print();
		}
*/


		ArrayList<Thread> threads = new ArrayList<Thread>();

		for(Runnable nextTrainer : trainers) {

			waitUntilNrOfThreadsBelow(threads, maxNrOfThreads);

			System.out.println("Starting new thread.");
			Thread trainerThread = new Thread(nextTrainer);
			trainerThread.start();
			threads.add( trainerThread );
		}

		waitUntilNrOfThreadsBelow(threads, 1);


		LocalTime finishTime = LocalTime.now();
		System.out.println("Total start time: " + startTime);
		System.out.println("Total finish time: " + finishTime);
	}

	/**
	 * Laat deze thread wachten tot het gewenste aantal threads of minder nog draait.
	 * @param threads 			Lijst van de alle threads
	 * @param maxNrOfThreads    Hoeveel threads maximaal tegelijk mogen draaien.
	 */
	public static void waitUntilNrOfThreadsBelow(ArrayList<Thread> threads, int maxNrOfThreads) {
		while( threads.size() >= maxNrOfThreads ) { //Probeer evenveel threads als (logische) cores te gebruiken
			//Controleer of er threads al klaar zijn
			for (Iterator<Thread> iterator = threads.iterator(); iterator.hasNext(); ) {
			    Thread element = iterator.next();

			    if( !element.isAlive() ) { //Als thread niet meer bezig is
			    	try{
			    		element.join(); //Join met main thread
			    		iterator.remove(); //Haal weg uit arraylist
			    		System.out.println("Joined with a thread.");
			    	} catch (InterruptedException ie) {
			    		ie.printStackTrace();
					}
			    }
			}

			if( threads.size() > 0 ) {
				//Wacht 2 seconde voordat opnieuw gecontroleerd wordt
				try {
					System.out.println(threads.size()+" threads still running, waiting 5 seconds.");
					Thread.sleep(5000);
				} catch(InterruptedException ie) {
					ie.printStackTrace();
					System.exit(-1); //Als main thread sleep niet kan aanroepen, omdat hij interupted is, is er iets raars aan de hand, dus exit met non-zero status.
				}
			}
		}
	}

	public ParallelTrainer(	String loadDirectory,
							String saveDirectory,
							String datasetDirectory,
							String[] datasetFilenames,
							int nrOfIterations,
							double learningRate,
							double targetError) {
		super(	loadDirectory,
				saveDirectory,
				datasetDirectory,
				datasetFilenames,
				nrOfIterations,
				learningRate,
				targetError);
	}

	public ParallelTrainer(	String loadDirectory,
							String saveDirectory,
							String datasetDirectory,
							String[] datasetFilenames,
							int nrOfIterations,
							double learningRate,
							double targetError,
							boolean combineThrottleAndBrake) { 
		super(	loadDirectory,
				saveDirectory,
				datasetDirectory,
				datasetFilenames,
				nrOfIterations,
				learningRate,
				targetError,
				combineThrottleAndBrake);
	}

	public void run() {
		trainSession();
	}

}