package stonks;

import ann.util.DataNormaliser;

/**
 * Abstracte klasse met daarin de functies die nodig zijn voor zowel
 * de DatasetCreator als de StonksDriver.
 */
public abstract class AbstractStonksDriver extends Controller{

	protected final static int DOWN = 0;		//Constanten voor het schakelen
	protected final static int UP = 1;

	protected double clutchPosition = 0.0;	//Houd positie van de koppeling bij. Begin met koppeling niet ingetrapt
	protected final static double clutchRelease = 0.05; //Geeft aan hoeveel de koppeling per iteratie los gelaten moet. Hoe lager, hoe langzamer.


	//Constanten voor de ABS uit SimpleDriver.
	final float wheelRadius[]={(float) 0.3306,(float) 0.3306,(float) 0.3276,(float) 0.3276};
	final float absSlip=(float) 2.0;
	final float absRange=(float) 3.0;
	final float absMinSpeed=(float) 3.0;

	protected final static double shiftRPM[][] = { //Bron voor shifts: De thesis "Q-learning with heuristic exploration in Simulated Car Racing" (pagina 28)
		//shift down @		shift UP @
		{ 0.0,				100.0,},	// 0 (N)
		{ 50.0,				8000.0,},	// 1
		{ 4000.0,			9500.0,},	// 2
		{ 6300.0,			9500.0,},	// 3
		{ 7000.0,			9500.0,},	// 4
		{ 7300.0,			9500.0,},	// 5
		{ 7300.0,			10000.0,},	// 6
	};

	
	/**
	 * Limiteert een waarde tot minimaal 0.0. In feite een constrain tot
	 * een range van (0.0, ->),
	 * @param value 	De waarde om te beperken.
	 * @return 			De waarde die nooit lager is dan 0.0
	 */
	protected static double notNegative(double value) {
		if (value < 0.0) {
			return 0.0;
		}
		return value;
	}


	/**
	 * Update het gaspedaal aan de hand van de koppeling. Terwijl koppeling opkomt,
	 * moet gas langzaam verder ingedrukt worden.
	 * @param action 	De action waarvan het gaspedaal aangepast gaat worden.
	 */
	protected void applyClutch(Action action) {
		if (action.accelerate > 0.0) {	//Geef geen gas als koppeling is ingedrukt
			action.accelerate = notNegative(action.accelerate - clutchPosition);
		}
	}

	/**
	 * Past ABS toe op de brake. Code overgenomen en klein beetje aangepast van SimpleDriver zoals op internet gegeven.
	 * @param sensors 	De sensorwaarden aan de hand waarvan de ABS status wordt bepaald.
	 * @param brake		De rem zoals ingedrukt.
	 * @return 			De rem zoals daadwerkelijk gebruikt moet worden na toepassen van ABS.
	 */
	protected float applyABS(SensorModel sensors, float brake){
		float speed = (float) (sensors.getSpeed() / 3.6);
		if (speed < absMinSpeed)
			return brake;

		float slip = 0.0f;
		for (int i = 0; i < 4; i++)
		{
			slip += sensors.getWheelSpinVelocity()[i] * wheelRadius[i];
		}
		slip = speed - slip/4.0f;
		if (slip > absSlip)
		{
			brake = brake - (slip - absSlip)/absRange;
		}

		if (brake<0)
			return 0;
		else
			return brake;
	}

	/**
	 * Berekent wat de juiste versnelling is om nu in te zitten. De RPMs
	 * waarop omhoog of omlaag geschakeld wordt staan in AbstractStonksDriver#shiftRPMs
	 * @param sensors 	De sensorwaarden waarvan de RPM gebruikt wordt om de versnelling te bepalen.
	 * @return 			De versnelling waarin de auto moet zitten.
	 */
	protected int calculateGear(SensorModel sensors) {
		if (sensors.getGear() < 0 ) {
			return 1;
		}

		if (clutchPosition > 0.0) {
			clutchPosition -= clutchRelease;
		} else {
			if (sensors.getRPM() < shiftRPM[sensors.getGear()][DOWN]) {
				clutchPosition = 1.0;
				return (sensors.getGear()-1);
			} else if (sensors.getRPM() > shiftRPM[sensors.getGear()][UP]) {
				clutchPosition = 1.0;
				return (sensors.getGear()+1);
			}
		}


		return sensors.getGear();
	}

	/**
	 * Variant van AbstractStonksDriver#sensorsToInputs met useLimitedSensors default op false.
	 * @param sensors 	De sensoren.
	 * @param inputs 	De array waarnaartoe de inputs voor het netwerk geschreven moeten worden.
	 */
	protected void sensorsToInputs(SensorModel sensors, double[] inputs) {
		sensorsToInputs(sensors, inputs, false);
	}


	/**
	 * Haalt de sensorwaarden uit het SensorModel object en stopt ze in de juiste volgorde in de
	 * input array. Krijgt ook een boolean die aangeeft of de extra sensoren gebruikt moeten worden
	 * Als deze boolean true is, dan worden de sensoren geordent zoals in de datasets van Elvira.
	 * Als deze boolean false is, dan worden ook de sensoren die Elvira niet gebruikt wel in de
	 * array gezet.
	 * Deze functie normaliseert nog <b>NIET</b>.
	 * @param sensors 	De sensoren.
	 * @param inputs 	De array waarnaartoe de inputs voor het netwerk geschreven moeten worden.
	 * @param useLimitedSensors Als true, dan worden LATERAL_SPEED, Z_SPEED, Z en WHEEL_SPIN_VELOCITY niet gebruikt.
	 */
	protected void sensorsToInputs(SensorModel sensors, double[] inputs, boolean useLimitedSensors) {
		inputs[DataNormaliser.SPEED] = sensors.getSpeed();
		inputs[DataNormaliser.TRACK_POSITION] = sensors.getTrackPosition();
		inputs[DataNormaliser.ANGLE_TO_TRACK_AXIS] = sensors.getAngleToTrackAxis();

		for(int i = 0; i < sensors.getTrackEdgeSensors().length; i++) {
			inputs[DataNormaliser.TRACK_EDGE+i] = sensors.getTrackEdgeSensors()[i];
		}

		if(!useLimitedSensors) {
			inputs[DataNormaliser.LATERAL_SPEED] = sensors.getLateralSpeed();
			inputs[DataNormaliser.Z_SPEED] = sensors.getZSpeed();
			inputs[DataNormaliser.Z] = sensors.getZ();

			for(int i = 0; i < sensors.getWheelSpinVelocity().length; i++) {
				inputs[DataNormaliser.WHEEL_SPIN_VELOCITY+i] = sensors.getWheelSpinVelocity()[i];
			}
		}
		
	}
}