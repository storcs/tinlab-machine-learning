package stonks;

import java.lang.Math;
import java.io.File;
import java.io.InputStream;
import java.util.Scanner;

import ann.network.NeuralNetwork;
import ann.util.NeuralNetworkBuilder;
import ann.util.DataNormaliser;
import ann.util.io.CSVReader;

/**
 * De Driver die gebruik maakt van een neuraal netwerk om te rijden.
 */
public class StonksDriver extends AbstractStonksDriver {
	private NeuralNetwork network; //Het neurale netwerk
	private double[] inputs; //Array waarin de inputs worden geschreven
	private double[] normalisedInputs; //Genormaliseerde inputs


	private final boolean useLimitedSensors = true; //Gebruik niet alle sensoren, maar alleen die uit de dataset van Elvira.
	private static final boolean combineThrottleAndBrake = true; //Combineer gas geven en remmen tot één output. Als true, dan wordt een netwerk met 2 outputs verwacht. Als false, dan 3 outputs.


	protected final static int STEERING	= 0,
							SPEED		= 1,
							SPRINGRATE	= 0,
							DAMPING		= 1,
							THRESHOLD	= 2,
							VFACTOR		= 3,
							LASTVAL		= 4;
	protected double[][][] opponentInfluence;


	final int  stuckTime = 25;
	final double  stuckAngle = 0.523598775; //PI/6
	private int stuck=0;
	RecoveryDriver recovery = new RecoveryDriver();

	public StonksDriver() {
		super(); //Roep voor de zekerheid constructor van base class
		int nrOfInputs = 29; //Gebruik standaard 29 inputs
		if(useLimitedSensors) { //Als true, dan minder sensoren gebruiken
			nrOfInputs = 22;
		}
		inputs = new double[nrOfInputs]; //Initialiseer arrays voor inputs
		normalisedInputs = new double[nrOfInputs];


		//Laad het netwerk uit de configuratiebestanden
		network = NeuralNetworkBuilder.loadNetwork("configuration/final/",	"config.txt");


		opponentInfluence = new double[36][2][5]; //De arrays voor het ontwijken van de tegenstanders
		try {
			readFromCSV(opponentInfluence, "configuration/final/opponentInfluence.csv");
		} catch(Exception e) {
			System.out.println("Could not read influences from CSV, using default influences instead.");
			opponentInfluence = defaultOpponentInfluence; //Gebruik default als niet uit csv gelezen kan worden.
		}
		

		//Controle of de instelling van combineThrottleAndBrake overeenkomt met de outputs
		double[] outputs = network.getOutputs(normalisedInputs); //Vraag een aantal outputs, om te kijken of netwerk 2 of 3 outputs geeft
		if(outputs.length == 2 && combineThrottleAndBrake) { //Als gecombineerd, dan moet aantal outputs 2 zijn
			System.out.println("Combining throttle and brake.");
		} else if (outputs.length == 3 && !combineThrottleAndBrake) { //Als niet gecombineerd, dan moet aantal outputs 3 zijn
			System.out.println("Not combining throttle and brake.");
		} else { //Stop direct als aantal outputs niet overeenkomt met verwachtte aantal
			System.out.println("Invalid amount of network outputs: "+outputs.length+" outputs, but combineThrottleAndBrake="+combineThrottleAndBrake+".");
			System.exit(-1);
		}

	}

	@Override
	public Action control(SensorModel sensors) {
		Action action = new Action();
		if (Math.abs(sensors.getAngleToTrackAxis()) > stuckAngle) {
			// update stuck counter
			stuck++;
		} else {
			// if not stuck reset stuck counter
			stuck = 0;
		}


		// after car is stuck for a while apply recovering policy
		if (stuck > stuckTime) {
			return recovery.control(sensors);
		} else {
			// Lees sensoren en zet waarden in input array:
			sensorsToInputs(sensors, inputs, useLimitedSensors);
			// Normaliseer alle inputs:
			DataNormaliser.normaliseInputs(inputs, normalisedInputs);

			// Vraag outputs van netwerk
			double[] outputs = network.getOutputs(normalisedInputs);


			//Schrijf outputs naar action:
			if(combineThrottleAndBrake) {
				
				//Combineert gaspedaal en rempedaal naar één output
				if(outputs[0] < 0.0) { //Als rem ingedrukt, dan:
					action.accelerate = 0.0; //Geen gas
					action.brake = (-1.0)*outputs[0]; //Rem ingedrukt is negatief, dus *-1
				} else { //Als geen rem ingedrukt
					action.accelerate = outputs[0]; //ACCELERATION
					action.brake = 0.0;
				}
				action.steering = outputs[1]; //STEERING is nu de tweede output, ipv de derde

			} else {

				//Combineert gaspedaal en rempedaal NIET, dus verwacht drie outputs.
				action.accelerate = outputs[0]; //ACCELERATION
				action.brake = outputs[1]; //BRAKE
				action.steering = outputs[2]; //STEERING
			}


			//Tel sturen en remmen voor de tegenstanders op bij de output van het netwerk.
			action.steering += getEvasionSteering(sensors);
			double tempSpeed = getEvasionSpeed(sensors);
			if(tempSpeed < 0.0) {
				if(action.accelerate > 0.0) {
					action.accelerate = 0.0;
					action.brake = Math.abs(tempSpeed);
				} else {
					action.brake += Math.abs(tempSpeed);
				}
			}

			//Functies voor het schakelen en de ABS toepassen:
			action.gear = calculateGear(sensors);
			applyClutch(action);
			action.brake = applyABS(sensors, (float) action.brake);

			return action;
		}
	}

	@Override
	public void reset() {
		//Er is niets om te resetten.
	}

	@Override
	public void shutdown() {
		//Er is niets om af te sluiten.
	}

	/**
	 * Doorloopt alle veren uit het massa-veer systeem en combineert ze tot één opgetelde waarde.
	 * @param influences 	De array met daarin alle benodigde constanten.
	 * @param sensors 		De sensoren die gebruikt moeten worden. Gebruik sensors.getOpponentSensors() voor tegenstanders, anders sensors.getTrackEdgeSensors()
	 * @param COLUMN 		Moet of STEERING of SPEED zijn.
	 * @param speed 		De huidige snelheid van de auto.
	 * @return 	De netto kracht die moet worden toegepast.
	 */
	private double applyForces(double[][][] influences, double[] sensors, int COLUMN, double speed) {
		double newVal = 0.0;
		double deltaVal = 0.0;
		double retVal = 0.0;
		for (int i = 0; i < sensors.length; i++) {

			//De kracht van de veer:
			newVal = notNegative( (influences[i][COLUMN][THRESHOLD] + influences[i][COLUMN][VFACTOR]*speed ) - sensors[i]) * influences[i][COLUMN][SPRINGRATE];
			
			//Het verschil met de laatste waarde:
			deltaVal = newVal - influences[i][COLUMN][LASTVAL];

			//Tel bij sturing op:	kracht van veer - de demping
			newVal -= ( influences[i][COLUMN][DAMPING] * deltaVal );
			retVal += newVal;
			influences[i][COLUMN][LASTVAL]= newVal;
		}
		return retVal;
	}

	/**
	 * De stuurcomponent voor het ontwijken van de tegenstanders.
	 * @return De waarde om op te tellen bij de output van het netwerk.
	 */
	private double getEvasionSteering(SensorModel sensors) {
		//Opponent sensors
		return applyForces(opponentInfluence, sensors.getOpponentSensors(), STEERING, sensors.getSpeed());
	}

	/**
	 * De component voor het gas geven en remmen voor het ontwijken van de tegenstanders.
	 * @return De waarde om op te tellen bij de output van het netwerk.
	 */
	private double getEvasionSpeed(SensorModel sensors) {
		//Opponent sensors
		return 1.0 - applyForces(opponentInfluence, sensors.getOpponentSensors(), SPEED, sensors.getSpeed());
	}

	/**
	 * Leest de constanten voor het massa-veer sys voor de tegenstanders.
	 * @param array 	De array waarnaartoe de waarden geschreven moeten worden.
	 * @param filename 	De bestandsnaam waarvandaan de waarden gelezen moete worden.
	 */
	protected void readFromCSV(double[][][] array, String filename) throws Exception {

		Scanner fileIn = null;
		if(CSVReader.useResourceStreams) {
			InputStream in = StonksDriver.class.getClassLoader().getResourceAsStream(filename);
			if(in == null) {
				throw new Exception("Could not open resource stream.");
			}
			fileIn = new Scanner(in).useDelimiter(CSVReader.DELIMITER);	
		} else {
			File csvFile = new File(filename);
			//Scanner met newlines als delimiter. Andere whitespace rond newline karakter wordt genegeerd.
			fileIn = new Scanner(csvFile).useDelimiter(CSVReader.DELIMITER);
		}


		for(int row = 0; row < array.length; row++ ) {	//Voor alle sensoren

			double[] nrs = CSVReader.readArrayFromCSV(fileIn);

			int index = 0;
			for(int i = 0; i < 2; i++) {	//Voor zowel sturen als gasgeven
				for(int col = 0; col < (array[row][i]).length; col++ ) {	//Ga alle waarden af
					
					array[row][i][col] = nrs[index];
					index++;
				}
			}
		}

		fileIn.close(); //Altijd afsluiten

	}

	///De default waarden om te gebruiken als niet uit het .csv bestand gelezen kan worden.
	private static final double defaultOpponentInfluence[][][] = {	//36 sensoren rondom
		//STEERING											SPEED
		//veer		demper	threshold 	Vfactor   lastval     | veer	demper	threshold 	Vfactor	lastval  
		{{-0.0,		0.5,	0.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//0
		{{-0.0,		0.5,	0.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//1
		{{-0.0,		0.5,	0.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//2
		{{-0.0,		0.5,	0.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//3
		{{-0.0,		0.5,	0.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//4
		{{-0.1,		0.5,	5.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//5
		{{-0.1,		0.5,	5.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//6
		{{-0.1,		0.5,	5.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//7
		{{-0.1,		0.5,	5.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//8
		{{-0.5,		0.6,	4.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//9
		{{-0.5,		0.6,	4.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//10
		{{-0.5,		0.6,	4.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//11
		{{-0.5,		0.6,	4.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//12
		{{-0.5,		0.6,	4.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//13
		{{-0.35,	0.6,	5.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//14
		{{-0.25,	0.5,	7.5,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//15
		{{-0.15,	0.5,	8.5,		0.01,		0.0},		{0.25,	0.7,	12.5,		0.0,	0.0},},	//16
		{{-0.10,	0.5,	8.5,		0.05,		0.0},		{0.5,	0.7,	12.5,		0.001,	0.0},},	//17	<--Midden
		{{0.10,		0.5,	8.5,		0.05,		0.0},		{0.5,	0.7,	12.5,		0.001,	0.0},},	//18	<--Midden
		{{0.15,		0.5,	8.5,		0.01,		0.0},		{0.25,	0.7,	12.5,		0.0,	0.0},},	//19
		{{0.25,		0.5,	7.5,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//20
		{{0.35,		0.6,	5.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//21
		{{0.5,		0.6,	4.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//22
		{{0.5,		0.6,	4.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//23
		{{0.5,		0.6,	4.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//24
		{{0.5,		0.6,	4.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//25
		{{0.5,		0.6,	4.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//26
		{{0.1,		0.5,	5.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//27
		{{0.1,		0.5,	5.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//28
		{{0.1,		0.5,	5.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//29
		{{0.1,		0.5,	5.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//30
		{{0.0,		0.5,	0.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//31
		{{0.0,		0.5,	0.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//32
		{{0.0,		0.5,	0.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//33
		{{0.0,		0.5,	0.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},},	//34
		{{0.0,		0.5,	0.0,		0.0,		0.0},		{0.0,	0.001,	0.0,		0.0,	0.0},}		//35
	};
}
