package stonks.input;

public abstract class CustomController {
    //0.0 als geen gas, 1.0 als vol gas
    public abstract double getAccelerate();
    //0.0 als niet remmen, 1.0 als vol remmen
    public abstract double getBrake();
    //-1.0 als maximaal naar rechts, 1.0 als maximaal naar links, 0.0 als recht vooruit
    public abstract double getSteering();
    //1 als omhoog schakelen, -1 als omlaag schakelen, 0 als niet schakelen
    public abstract int getGearChange();

    public abstract void poll();

    public abstract boolean initializeGamepad();

    public abstract void close();
}
