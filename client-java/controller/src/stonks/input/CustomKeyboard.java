package stonks.input;

import javax.swing.*;
import java.awt.event.*;

public class CustomKeyboard extends CustomController {
	public static void main (String[] args) {

		CustomKeyboard keyboard = new CustomKeyboard();

		if(keyboard.initializeGamepad()) {
			int gear = 0;
			if(true) {
				keyboard.poll();
				gear+=keyboard.getGearChange();
				System.out.println("Accel:\t" + keyboard.getAccelerate()+
									"\tBrake:\t" + keyboard.getBrake()+
									"\tSteering:\t" + keyboard.getSteering()+
									"\tGear = \t" + gear);
			}
		} else {
			System.out.println("Failed to initialize gamepad");
		}
	}



	private boolean accelerate = false;
	private boolean brake = false;
	private boolean steerLeft = false;
	private boolean steerRight = false;
	private float steerValue = 0;
	private boolean shiftDown = false;
	private boolean shiftUp = false;

	private JFrame frame;
	//True als gelukt is, anders false
	public boolean initializeGamepad() {
		frame = new JFrame("Test123");

		frame.setSize(400,400);
		frame.setVisible(true);

		frame.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent event) {
                if (event.getKeyChar() == 'w') {
                	accelerate = true;
                } else if (event.getKeyChar() == 's') {
                	brake = true;
                } else if (event.getKeyChar() == 'a') {
                	steerLeft = true;
                } else if (event.getKeyChar() == 'd') {
                	steerRight = true;
                } else if (event.getKeyChar() == 'y') {
                	shiftDown = true;
                } else if (event.getKeyChar() == 'u') {
                	shiftUp = true;
                }
            }

            @Override
            public void keyReleased(KeyEvent event) {
				if (event.getKeyChar() == 'w') {
                	accelerate = false;
                } else if (event.getKeyChar() == 's') {
                	brake = false;
                } else if (event.getKeyChar() == 'a') {
                	steerLeft = false;
                } else if (event.getKeyChar() == 'd') {
                	steerRight = false;
                } else if (event.getKeyChar() == 'y') {
                	shiftDown = false;
                } else if (event.getKeyChar() == 'u') {
                	shiftUp = false;
                }
            }

            @Override
            public void keyTyped(KeyEvent event) { }
        });

        return true;

	}


	public void poll() {}
	public void close() {
		frame.dispose();
	}


	//0.0 als geen gas, 1.0 als vol gas
	public double getAccelerate() {
		if(accelerate) {
			return 1.0;
		}
		return 0.0;
	}
	//0.0 als niet remmen, 1.0 als vol remmen
	public double getBrake() {
		if(brake) {
			return 1.0;
		}
		return 0.0;
	}
	//-1.0 als maximaal naar rechts, 1.0 als maximaal naar links, 0.0 als recht vooruit
	public double getSteering() {
		if(steerLeft) {
			if (steerValue <= 1.0) steerValue += 0.03;
		} else if(steerRight) {
			if (steerValue >= -1.0) steerValue -= 0.03;
		} else {
			steerValue = 0;
		}
		return steerValue;
	}
	//1 als omhoog schakelen, -1 als omlaag schakelen, 0 als niet schakelen
	public int getGearChange() {
		if(shiftDown) {
			return -1;
		} else if (shiftUp) {
			return 1;
		}
		return 0;
	}
}