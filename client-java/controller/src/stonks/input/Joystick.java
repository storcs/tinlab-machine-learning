package stonks.input;

import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Event;
import net.java.games.input.EventQueue;

public class Joystick extends CustomController {
	public static void main (String[] args) {

		Joystick joystick = new Joystick();

		if(joystick.initializeGamepad()) {
			int gear = 0;
			while(true) {
				joystick.poll();
				gear+=joystick.getGearChange();
				System.out.println("Accel:\t" + joystick.getAccelerate()+
									"\tBrake:\t" + joystick.getBrake()+
									"\tSteering:\t" + joystick.getSteering()+
									"\tGear = \t" + gear);
			}
		} else {
			System.out.println("Failed to initialize gamepad");
		}

		
			

	}


	private Controller controller = null;
	private Component[] components = null;
	
	private int accellerateIndex = -1;
	private int brakeIndex = -1;
	private int steeringIndex = -1;
	private int shiftDownIndex = -1;
	private int shiftUpIndex = -1;

	private boolean shiftDownPressed = false;
	private boolean shiftUpPressed = false;

	private final double accellerateDeadzone = 0.05;
	private final double brakeDeadzone = 0.05;

	//True als gelukt is, anders false
	public boolean initializeGamepad() {
		controller = null;
		System.out.println("Requesting all controllers");
		Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();

		for (int i = 0; i < controllers.length; i++) {
			if(controllers[i].getType().toString().equals("Gamepad")) {
				System.out.println("Found gamepad");
				this.controller = controllers[i];
			}
		}

		if(controller == null) {
			return false;
		}

		components = controller.getComponents();
		if(components == null) {
			return false;
		}
		for(int j=0;j<components.length;j++){
			//System.out.println("Component "+j+": "+components[j].getName());	
			if(components[j].getName().equals("Z-draaiing")) {
				accellerateIndex = j;
				brakeIndex = j;
			} else if(components[j].getName().equals("X-as")) {
				steeringIndex = j;
			} else if(components[j].getName().equals("Knop 4")) {
				shiftDownIndex = j;
			} else if(components[j].getName().equals("Knop 5")) {
				shiftUpIndex = j;
			} else {
				System.out.println("Component "+j+": "+components[j].getName());
			}

		}

		if( accellerateIndex == -1 || brakeIndex == -1 || steeringIndex == -1 || shiftDownIndex == -1 || shiftUpIndex == -1 ) {
			return false;
		} else {
			return true;
		}

	}

	public void printComponentValue(Component component) {
		System.out.print(component.getName()+": ");
		if( component.isAnalog() ) {
			System.out.println(component.getPollData());
		} else {
			if( component.getPollData() == 0.0f ) {
				System.out.println("FALSE");
			} else {
				System.out.println("TRUE");
			}
		}
	}

	public void poll() {
		controller.poll();
	}
	public void close() {}

	//0.0 als geen gas, 1.0 als vol gas
	public double getAccelerate() {
		if(components[accellerateIndex].getPollData() < -accellerateDeadzone ) { //Gecombineerde as met gaspedaal, waarbij positieve waarden staan voor remmen
			return Math.abs(components[accellerateIndex].getPollData());
		}
		return 0.0;
	}
	//0.0 als niet remmen, 1.0 als vol remmen
	public double getBrake() {
		if(components[brakeIndex].getPollData() > brakeDeadzone ) { //Gecombineerde as met gaspedaal, waarbij positieve waarden staan voor remmen
			return components[brakeIndex].getPollData();
		}
		return 0.0;
	}
	//-1.0 als maximaal naar rechts, 1.0 als maximaal naar links, 0.0 als recht vooruit
	public double getSteering() {
		//return - components[steeringIndex].getPollData();
		if(components[steeringIndex].getPollData() < 0.0) {
			return 0.75 * Math.pow(  Math.abs(components[steeringIndex].getPollData()), 1.4);
		}
		return -0.75 * Math.pow(components[steeringIndex].getPollData(), 1.4);
		
	}
	//1 als omhoog schakelen, -1 als omlaag schakelen, 0 als niet schakelen
	public int getGearChange() {
		if(components[shiftDownIndex].getPollData() == 1.0f) {
			if(!shiftDownPressed) { //Geef alleen signaal als knop niet vorige iteratie ook al ingedrukt was
				shiftDownPressed = true; //Set op true
				return -1;
			}
		} else {
			shiftDownPressed = false;
		}

		if(components[shiftUpIndex].getPollData() == 1.0f) {
			if(!shiftUpPressed) { //Geef alleen signaal als knop niet vorige iteratie ook al ingedrukt was
				shiftUpPressed = true; //Set op true
				return 1;
			}
		} else {
			shiftUpPressed = false;
		}
		return 0;
	}
}