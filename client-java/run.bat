::Scherm leeg
cls

@echo off

SET branchName=diy-neural-network

::Directories voor JInput:
::Directory met "jinput-2.0.9.jar"
SET jinputDir=../../somefiles/jinput/\*
::Directory met jinput-dx8_64.dll, jinput-raw_64.dll en jinput-wintab.dll:
SET jinputDir=%jinputDir%;../../somefiles/jinput/jinput-2.0.9-natives-all/

::Directory waar de class files staan:
SET classDir=classes
::Classpath is directory met eigen .class files en de JInput jar:
SET classPath=%classDir%;%jinputDir%

::De verschillende directories met source files:
SET src=controller/src/stonks/*.java

::Voeg alleen toe wanneer op juiste branch:
IF %branchName%==diy-neural-network ( SET src=%src% controller/src/ann/math/linearmath/*.java )
IF %branchName%==diy-neural-network ( SET src=%src% controller/src/ann/math/function/*.java )
IF %branchName%==diy-neural-network ( SET src=%src% controller/src/ann/util/io/*.java )
IF %branchName%==diy-neural-network ( SET src=%src% controller/src/ann/util/*.java )
IF %branchName%==diy-neural-network ( SET src=%src% controller/src/ann/network/*.java )
IF %branchName%==diy-neural-network ( SET src=%src% controller/src/stonks/input/*.java )
IF %branchName%==diy-neural-network ( SET src=%src% controller/src/stonks/optimize/*.java )

@echo on

::Daadwerkelijk compileren
::De folder voor .class files is zowel onderdeel van de classpath als de output directory
::Let op dat als dat een classname veranderd wordt, dat de oude .class file verwijderd wordt,
::anders wordt de oude .class file nog gebruikt bij compilatie van andere modules.
echo "classPath: %classPath%"

echo "classDir: %classDir%"
echo "src: %src%"

javac -cp %classPath% -d %classDir% %src%

@echo off
::Alle class files die in de jar gezet moeten worden. Zijn alleen de nodige bestanden, dus niet allemaal.
SET classFiles=ann/math/function/ActivationFunction.class
SET classFiles=%classFiles% ann/math/function/ErrorFunction.class
SET classFiles=%classFiles% ann/math/function/HyperbolicTangent.class
SET classFiles=%classFiles% ann/math/function/Linear.class
SET classFiles=%classFiles% ann/math/function/MeanSquaredError.class
SET classFiles=%classFiles% ann/math/function/Sigmoid.class
SET classFiles=%classFiles% ann/math/function/Softmax.class

SET classFiles=%classFiles% ann/math/linearmath/LinearMath.class

SET classFiles=%classFiles% ann/util/DataNormaliser.class
SET classFiles=%classFiles% ann/util/io/CSVReader.class
SET classFiles=%classFiles% ann/util/NeuralNetworkBuilder.class

SET classFiles=%classFiles% ann/network/NeuralNetwork.class
SET classFiles=%classFiles% ann/network/NeuronLayer.class

SET classFiles=%classFiles% stonks/AbstractStonksDriver.class
SET classFiles=%classFiles% stonks/Action.class
SET classFiles=%classFiles% stonks/Client.class
SET classFiles=%classFiles% stonks/Controller$Stage.class
SET classFiles=%classFiles% stonks/Controller.class
SET classFiles=%classFiles% stonks/Loader.class
SET classFiles=%classFiles% stonks/DeadSimpleSoloController.class
SET classFiles=%classFiles% stonks/MessageBasedSensorModel.class
SET classFiles=%classFiles% stonks/MessageParser.class
SET classFiles=%classFiles% stonks/RecoveryDriver.class
SET classFiles=%classFiles% stonks/SensorModel.class
SET classFiles=%classFiles% stonks/SimpleDriver.class
SET classFiles=%classFiles% stonks/SocketHandler.class
SET classFiles=%classFiles% stonks/StonksDriver.class

SET extraFiles=../configuration/final/config.txt
SET extraFiles=%extraFiles% ../configuration/final/opponentInfluence.csv
SET extraFiles=%extraFiles% ../configuration/final/layers/layer0.csv
SET extraFiles=%extraFiles% ../configuration/final/layers/layer1.csv
SET extraFiles=%extraFiles% ../configuration/final/layers/layer2.csv
@echo on

cd %classDir%
jar cfm ../bin/driver.jar ../Manifest.txt %classFiles% %extraFiles%
cd ../

::Run
::NeuralNetTest.main() uitvoeren:
::java ann.util.NeuralNetTest
::java ann.util.CreateNetwork

::NetworkTrainer.main() uitvoeren:
::java ann.util.ParallelTrainer
::java ann.util.NetworkTrainer

::Joystick testen:
::java -cp %classPath% -Djava.library.path=%jinputDir% stonks.Joystick

::Normaliseer csv's
::java ann.util.DataNormaliser

::Creeren van data sets met stuurwiel
::java -cp %classPath% -Djava.library.path=%jinputDir% stonks.Client stonks.DatasetCreator host:127.0.0.1 port:3001

::Driver runnen met restarts
::java stonks.Client stonks.StonksDriver host:127.0.0.1 port:3001 maxEpisodes:50
::Driver runnen:
::java stonks.Client stonks.StonksDriver host:127.0.0.1 port:3001
::Learning driver runnen
::java stonks.Client stonks.optimize.OpponentLearningStonksDriver host:127.0.0.1 port:3001 maxEpisodes:50


cd bin
::java -jar driver.jar
cd ../


::git log --graph > ../deliverableGroeps/Groepsdeel/git_log.txt
