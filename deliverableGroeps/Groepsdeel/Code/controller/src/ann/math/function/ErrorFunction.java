package ann.math.function;

public abstract class ErrorFunction {
	public abstract double getError(double actualOutput, double desiredOutput);

	/**
	 * De partiele afgeleide van Error naar de activation(=actualOutput)
	 * actualOutput = activation = a_i
	 * desiredOutput = t_i
	 */
	public abstract double errorDerivToActivation(double actualOutput, double desiredOutput);
}