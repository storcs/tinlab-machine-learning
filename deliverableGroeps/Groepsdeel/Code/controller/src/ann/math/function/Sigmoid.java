package ann.math.function;

/**
 * Implementatie van de sigmoid functie
 */
public class Sigmoid extends ActivationFunction {
	/**
	 * Een look-up table zou sneller kunnen zijn, maar vooralsnog voldoet het telkens berekenen.
	 */
	public double f(double z) {
		return 1.0 / (1.0 + Math.exp(-z));
	}

	public double fDeriv(double z) {
		return f(z) * (1 - f(z));
	}
}