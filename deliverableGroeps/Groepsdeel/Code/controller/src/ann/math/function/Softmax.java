package ann.math.function;

public class Softmax extends ActivationFunction {

	/**
	 * Past de softmax functie toe op de kolomvector a. Houdt geen rekening met overflow, alhoewel
	 * dat wel een risico is bij de som van de exponenten.
	 * Mocht overflow een probleem blijken, dan is hier misschien een oplossing: https://lingpipe-blog.com/2009/06/25/log-sum-of-exponentials/
	 */


	/**
	 * In dit geval kan f niet gebruikt voor losse element. Om problemen met de compiler te
	 * voorkomen moet er toch een lege body aan gegeven worden...
	 */
	public double f(double z) { return 0.0; };


	public double fDeriv(double z) {
		throw new RuntimeException("Softmax derivative not implemented...");
	}
}