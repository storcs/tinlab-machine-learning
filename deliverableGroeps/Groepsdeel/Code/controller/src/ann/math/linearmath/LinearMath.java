package ann.math.linearmath;

public class LinearMath {
	
	/*
	 * Transponeert een matrix (spiegelt in de diagonaal)
	 */
	public static double[][] transpose(double[][] matrix) {
		double[][] transposed = new double[matrix[0].length][matrix.length];

		for(int i = 0; i < matrix.length; i++) {
			for(int j = 0; j < matrix[0].length; j++) {
				transposed[j][i] = matrix[i][j];
			}
		}

		return transposed;
	}

	/**
	 * Dot product van 2 matrices (a*b)
	 * Een MxN matrix is is een matrix met M rijen en N kolommen.
	 * Elementen worden gebruikt met matrix[r][k] met r de rij en k de kolom.
	 * Een matrix is dus:
	 * { {a01, a02, ... , a0n},
	 *	 {a11, a12, ... , a1n},
	 *	 {..., ..., ... , ...},
	 *	 {am1, am2, ... , amn}, }
	 *
	 * A is een MxN matrix en B is een NxP matrix.
	 * Er wordt gecontroleerd dat aantal kolommen in A hetzelfde is
	 * als het aantal rijen in B. Dit wordt niet voor alle rijen en
	 * kolommen gedaan, maar alleen voor de eerste rij van A en de
	 * eerste kolom van B.
	 * Index out of range exception als niet alle rijen dezelfde
	 * lengte hebben.
	 */
	public static double[][] multiply(double[][] a, double[][] b) {
		if(a[0].length != b.length) {
			throw new RuntimeException("Matrix sizes do not match. (A:" +a.length+"x"+a[0].length+ ", B:" +b.length+"x"+b[0].length+ ")");
		}

		double[][] retMatrix = new double[a.length][b[0].length]; //Noem return matrix M



		//Matrix vermenigvuldiging wordt met for loops gedaan. Dit zou een perfecte operatie zijn
		//om door een GPU uit te laten voeren, maar zulke low-level control is met Java waarschjinlijk
		//niet te bereiken. In plaats daarvan vertrouw ik er op dat de compiler of JVM slim genoeg is
		//om gebruik te maken van vector units van de CPU mbv wat loop-unrolling.
		for(int mRow = 0; mRow < retMatrix.length; mRow++ ) { //Ga alle rijen van M af.
			for(int mCol = 0; mCol < retMatrix[mRow].length; mCol++ ) { //Ga alle kolommen van M af
				double value = 0.0;

				//Bereken het product:
				for(int i = 0; i < a[mRow].length; i++ ) { //i van 0 tot a[mRow].length of b.length (maakt niet uit, want toch hetzelfde)
					//Ga per element rij van matrix A en kolom van matrix B af. Vermenigvuldig met elkaar en tel op bij totaal;
					value += a[mRow][i] * b[i][mCol] ;
				}

				retMatrix[mRow][mCol] = value;
			}
		}

		return retMatrix;
	}

	/**
	 * Vermenigvuldigd elk element in de matrix a met de scalair s.
	 */
	public static double[][] multiply(double s, double[][] a) {
		double[][] retMatrix = new double[a.length][a[0].length];

		for(int row = 0; row < retMatrix.length; row++) {
			for(int col = 0; col < retMatrix[row].length; col++) {
				retMatrix[row][col] = s*a[row][col];
			}
		}

		return retMatrix;
	}

	/*
	 * Telt twee matrices bij elkaar op.
	 */
	public static double[][] add(double[][] a, double[][] b) {
		if(a.length != b.length || a[0].length != b[0].length) {
			throw new RuntimeException("Matrix sizes do not match. (A:" +a.length+"x"+a[0].length+ ", B:" +b.length+"x"+b[0].length+ ")");
		}

		double[][] retMatrix = new double[a.length][a[0].length]; //Noem return matrix M

		for(int mRow = 0; mRow < retMatrix.length; mRow++ ) { //Ga alle rijen van M af.
			for(int mCol = 0; mCol < retMatrix[mRow].length; mCol++ ) { //Ga alle kolommen van M af

				retMatrix[mRow][mCol] = a[mRow][mCol] + b[mRow][mCol];

			}
		}

		return retMatrix;
	}

	/*
	 * Print een matrix naar de console
	 */
	public static void print(double[][] matrix) {
		for(int r = 0; r < matrix.length; r++) {
			for(int c = 0; c < matrix[r].length; c++) {
				System.out.print("\t"+ matrix[r][c] +",");
			}
			System.out.println();
		}
	}
}