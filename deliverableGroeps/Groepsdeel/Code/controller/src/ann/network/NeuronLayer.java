package ann.network;

import ann.math.function.ActivationFunction;

/**
 * Een simpele klasse, die in staat stelt om verschillende activation functions
 * voor verschillende lagen te gebruiken en die de bestandsnaam waarnaar
 * de waarden opgeslagen moeten worden bij de biases zelf houdt.<br>
 * <br>
 * Members zijn expres public gemaakt om de klasse simpel te houden. Denk
 * aan een C struct.
 */
public class NeuronLayer {
	/**
	 * Een matrix met daarin de gewichten van elke verbinding tussen twee
	 * de twee lagen <i>L</i> en <i>L+1</i> van het neurale netwerk.
	 * Dit is een MxN matrix (dus M rijen en N kolommen). Hierbij heeft
	 * laag <i>L</i> in totaal N neuronen en heeft laag <i>L+1</i> totaal
	 * M neuronen.<br>
	 * De matrix moet dus double[M][N] groot zijn, want de elementen worden
	 * als weight[rij][kolom] gebruikt.<br>
	 * Het gewicht op plaats
	 */
	public double[][] weight = null;

	//De activation function die voor alle neuronen in deze layer gebruikt wordt.
	//public ActivationFunction f = null;
	public ActivationFunction[] f = null;

	//path wordt momenteel nog niet gebruikt
	public String path = null;
}