package ann.util;

import java.io.File;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.time.LocalTime;

import ann.network.NeuralNetwork;
import ann.util.io.CSVReader;

public abstract class AbstractTrainer {

	private String loadDirectory;
	private String saveDirectory;
	private String datasetDirectory;
	private String[] datasetFilenames;
	private int nrOfIterations;
	private double targetError;
	private double learningRate;

	private boolean combineThrottleAndBrake = false;

	/**
	 * Maak een nieuwe trainer aan met combineThrottleAndBrake default op false.
	 * @param	loadDirectory			De directory waarin gezocht wordt naar het "files.txt" waarvandaan het netwerk wordt geladen.
	 * @param	saveDirectory			De directory waarnaar het netwerk wordt opgeslagen.
	 * @param	datasetDirectory		De directory waarin de datasets staan.
	 * @param	datasetFilenames		Een array met daarin de bestandsnamen van de datasets.
	 * @param	nrOfIterations			Hoevaak elke dataset getraind moet worden. Als targetError groter is dan 0.0, dan is nrOfIterations de hoeveelheid iteraties waarna het netwerk naar een tijdelijke map wordt opgeslagen.
	 * @param	learningRate			De learning rate om te gebruiken bij het trainen.
	 * @param	targetError				De error waaronder het netwerk moet komen. Als de error 0.0 is, dan wordt slechts nrOfIterations keer getraind, ongeacht de error.
	 * @param	combineThrottleAndBrake	Een boolean die aangeeft of gas en rem tot één as gecombineerd moeten worden.
	 */
	public AbstractTrainer(	String loadDirectory,
							String saveDirectory,
							String datasetDirectory,
							String[] datasetFilenames,
							int nrOfIterations,
							double learningRate,
							double targetError) {
		this.loadDirectory = loadDirectory;
		this.saveDirectory = saveDirectory;
		this.datasetDirectory = datasetDirectory;
		this.datasetFilenames = datasetFilenames;
		this.nrOfIterations = nrOfIterations;
		this.learningRate = learningRate;
		this.targetError = targetError;
		this.combineThrottleAndBrake = false; //Standaard false
	}
	public AbstractTrainer(	String loadDirectory,
							String saveDirectory,
							String datasetDirectory,
							String[] datasetFilenames,
							int nrOfIterations,
							double learningRate,
							double targetError,
							boolean combineThrottleAndBrake) { 
		this(loadDirectory, saveDirectory, datasetDirectory, datasetFilenames, nrOfIterations, learningRate, targetError);
		this.combineThrottleAndBrake = combineThrottleAndBrake;
	}

	protected void print() {
		System.out.println("From '"+loadDirectory+"' to '"+saveDirectory+"' after "+nrOfIterations+" with rate "+learningRate);
	}
	
	protected int getNrOfIterations() {
		return nrOfIterations;
	}

	/**
	 * Laad het netwerk, doorloopt alle bestanden het gegeven
	 * aantal keer en slaat netwerk weer op.
	 * Wordt afgebroken en niet opgeslagen bij NaN.
	 */
	protected void trainSession() {
		NeuralNetwork net = NeuralNetworkBuilder.loadNetwork(loadDirectory, "files.txt");
		LocalTime startTime = LocalTime.now(); //Houd tijd bij

		if(combineThrottleAndBrake) { //Verbose
			System.out.println("Combining throttle and brake");
		} else {
			System.out.println("Not combining throttle and brake");
		}

		int startingIterationCount = nrOfIterations;

		for(int i = 0 ; i < nrOfIterations; i++) {
			double avgError = 0.0; //Gemiddelde error van alle bestanden

			ArrayList<String> datasetList = new ArrayList<String>(); //Maak een lijst van alle bestanden, zodat hij geshuffled kan worden.
			for(String filename : datasetFilenames) {
				datasetList.add(filename);
			}
			Collections.shuffle(datasetList); //Suffle de bestanden, zodat willekeurige volgorde gebruikt wordt.

			for(String filename : datasetList) {
				try { //Train elk bestand:
					avgError+= train(net, datasetDirectory+filename, learningRate, combineThrottleAndBrake);

				} catch (Exception e) {
					System.out.println("Exception when training '"+filename+"' (iteration "+i+").");
					e.printStackTrace();
					return;
				}
			}

			//Bereken gemiddelde error:
			avgError /= datasetList.size();

			if(targetError > 0.0) { //Als target error is ingesteld:
				System.out.println("Target error: "+targetError+",\tActual error: "+avgError+".");
				if(avgError > targetError) { //Als error groter is dan target error:
					if(i>=nrOfIterations-1) {
						System.out.println("Error still too high: doing another iteration ("+(i+1)+").");
						nrOfIterations++; //DOe nog een iteratie door ALLE bestanden
					}
				} else {
					i = nrOfIterations;
				}
				if(i % startingIterationCount == 0) {
					try {
						//System.out.println( saveDirectory.substring(0, saveDirectory.lastIndexOf("/"))+"_temp/" );
						NetworkSaver.saveNetwork(net, saveDirectory.substring(0, saveDirectory.lastIndexOf("/"))+"_temp/" );
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				System.out.println("Completed iteration "+i+".");
			}

		}

		try { //Probeer netwerk na afloop op te slaan:
			NetworkSaver.saveNetwork(net, saveDirectory);
		} catch(Exception e) {
			e.printStackTrace();
		}

		LocalTime finishTime = LocalTime.now();

		System.out.println("Session start time: " + startTime);
		System.out.println("Session finish time: " + finishTime);
	}

	/**
	 * Traint een netwerk met een bestand met de gegeven locatie en learning rate.
	 */
	public static double train(	NeuralNetwork network,
						String dataSetPath,
						double learningRate,
						boolean combineThrottleAndBrake ) throws Exception {

		File file = new java.io.File(dataSetPath);
		Scanner fileIn = new java.util.Scanner(file).useDelimiter(CSVReader.DELIMITER);

		int linesDone = 0;
		double[] nextInputLine = CSVReader.readArrayFromCSV(fileIn);
		double[] inputs = new double[ nextInputLine.length-3 ];
		List<double[]> allLines = new ArrayList<double[]>();
		
		while(nextInputLine != null) {
			allLines.add(nextInputLine);

			nextInputLine = CSVReader.readArrayFromCSV(fileIn);
		}
		fileIn.close();

		Collections.shuffle(allLines);



		
		double[] outputs = null;
		double[] desiredOutputs = new double[3];
		double[] combinedDesiredOutputs = new double[2];


		int errorIndex = -1;
		double[] error = new double[250]; //Lengte van error array is over hoeveel van de laatste lines de gemiddelde error berekend wordt


		for(double[] nextLine : allLines) {
			//Schrijf laatste elementen van regel naar input array
			for(int i = 0; i < inputs.length; i++) {
				inputs[i] = nextLine[ i+3]; //Schrijf alle sensoren naar inputs.
			}

			//Vraag outputs van netwerk op
			outputs = network.getOutputs(inputs);

			//De gewenste outputs staan als eerste in de csv:
			for(int i = 0; i < 3; i++) {
				desiredOutputs[i] = nextLine[i];
			}

			//Backpropagate door het netwerk
			if(combineThrottleAndBrake) {
				combinedDesiredOutputs[0] = desiredOutputs[0] - desiredOutputs[1]; //ACCELERATE - BRAKE
				combinedDesiredOutputs[1] = desiredOutputs[2]; //Schuif STEERING een plaatsje op
				error[(++errorIndex)%(error.length)] = network.updateWeights(outputs, combinedDesiredOutputs, learningRate);
			} else {
				error[(++errorIndex)%(error.length)] = network.updateWeights(outputs, desiredOutputs, learningRate);
			}
			

			linesDone++;


			for(int i = 0; i < outputs.length; i++) {
				if( Double.isNaN(outputs[i]) ) {
					throw new Exception("Nan for output "+i+" after "+linesDone+" lines from '"+dataSetPath+"'.");
				}
			}
		}	

		
		//System.out.println("Completed '"+dataSetPath+"'");
		//sessions++;



		//System.out.println("Training session "+sessions+ " completed.");

		double avgError = 0.0;
		for(double d : error) {
			avgError+=d;
		}
		avgError /= error.length;
		//System.out.println("File error: "+avgError);

		return avgError;
	}
}