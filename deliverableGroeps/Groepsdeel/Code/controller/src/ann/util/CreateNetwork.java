package ann.util;


import ann.network.NeuralNetwork;
import ann.util.NeuralNetworkBuilder;
import ann.util.NetworkSaver;
import ann.util.io.CSVReader;


/**
 * Klasse waarin netwerk wordt aangemaakt.
 * Hoeft maar eenmaal gebruikt.
 */
public class CreateNetwork {
	public static void main(String[] args) {
		int nrOfLayers = 3;
		NeuralNetwork network = NeuralNetworkBuilder.createNewNetwork(nrOfLayers);

		NeuralNetworkBuilder.addNewLayer(network,
									0, //Index (0-based)
									22, //Inputs
									60, //Aantal neuronen in deze laag
									CSVReader.TANH, //String met activation functie
									NeuralNetworkBuilder.XAVIER_INIT); //De manier van initialiseren van de gewichten

		NeuralNetworkBuilder.addNewLayer(network,
									1, //Index (0-based)
									60, //Inputs
									60, //Aantal neuronen in deze laag
									CSVReader.TANH, //String met activation functie
									NeuralNetworkBuilder.XAVIER_INIT); //De manier van initialiseren van de gewichten

		NeuralNetworkBuilder.addNewLayer(network,
									2, //Index (0-based)
									60, //Inputs
									2, //Aantal neuronen in deze laag
									CSVReader.LINEAR, //String met activation functie
									NeuralNetworkBuilder.XAVIER_INIT); //De manier van initialiseren van de gewichten

		NeuralNetworkBuilder.setNeuronActivationFunction(network,
												2,	//Laag
												0,	//Neuron
												CSVReader.TANH); //Functie


		if(!network.isComplete()) {
			throw new RuntimeException("traalala");
		}



		try {
			NetworkSaver.saveNetwork(network, "configuration/elvira-dataset-test/2-hidden_60-tanh_60-tanh_tanh+linear-output/untrained/");
		} catch(Exception e) {
			e.printStackTrace();
		}


		network = NeuralNetworkBuilder.loadNetwork("configuration/elvira-dataset-test/2-hidden_60-tanh_60-tanh_tanh+linear-output/untrained/", "files.txt");
		//network.printLayers();
		System.out.println("jaja");

	}
}