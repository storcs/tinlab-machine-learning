package ann.util;


import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

import ann.util.io.CSVReader;


/**
 * Klasse om data mee te normaliseren.
 */
public class DataNormaliser {
	/**
	 * Deze main neemt alle datasets van Elvira en normaliseert ze.
	 */
	public static void main(String[] args) {
		//DataNormaliser dn = new DataNormaliser();
		
		//Directory waarvandaan csv's gelezen gaan worden:
		String notNormalisedDir = "../../train_data/clean/";

		//Directory waarnaartoe csv's geschreven gaan worden:
		String normalisedDir = "../train_data/normalised/";

		//Alle bestanden die genormaliseerd gaan worden:
		String[] files = {	"aalborg.csv",
							"alpine-1.csv",
							"Alpine_Track_1.csv",
							"combined_data.csv",
							"Corners.csv",
							"Corners_2.csv",
							"Corners_3.csv",
							"Corners_4.csv",
							"Corners_version_2.csv",
							"Corners_version_2_1.csv",
							"f-speedway.csv",
							"speedway_left_mixed.csv",
							"sprint.csv",
							"suzuka.csv" };

		//Ga alle bestanden af en normaliseer:
		for(String filename : files) {
			DataNormaliser.normalise(	notNormalisedDir+filename,
							normalisedDir+filename);
		}


		notNormalisedDir = "../train_data/";
		normalisedDir = "../train_data/normalised/";
		files = new String[]{	"aalborg_2laps_total2.35_best1.14.csv",
								"alpine_2laps_total4.18_best2.06_0dmg.csv",
								"alpine_2laps_total4.21_best2.07_212dmg.csv",
								"alpine_2laps_total4.25_best2.09_231dmg.csv",
								"alpine_2laps_total4.30_best2.11-47dmg.csv",
								"CG_Speedway_1_3laps_total2.08_best0.40.csv",
								"CG_Speedway_number_1.csv",
								"CG_track_2_2laps_total1.55_best0.54.csv",
								"CG_track_2_3laps_total2.46_best0.52.csv",
								"CG_track_3_3laps_total3.17_best1.03.csv",
								"Ruudskogen_total2.17_best1.06.csv",
								"Street_1_total2.47_best1.20.csv" };

		for(String filename : files) {
			DataNormaliser.normalise(	notNormalisedDir+filename,
							normalisedDir+filename);
		}

	}



	public static int SPEED	= 0,
			TRACK_POSITION		= 1,
			ANGLE_TO_TRACK_AXIS	= 2,
			TRACK_EDGE			= 3,
			LATERAL_SPEED		= 22,
			Z_SPEED				= 23,
			Z					= 24,
			WHEEL_SPIN_VELOCITY	= 25;

	public static void normalise(String inputCSVpath, String outputCSVpath) {
		try {

			File file = new java.io.File(inputCSVpath);
			Scanner fileIn = new java.util.Scanner(file).useDelimiter(CSVReader.DELIMITER);

			file = new File(outputCSVpath);
			PrintWriter csvOut = new PrintWriter(file); //Open het bestand


			System.out.println("Reading from file '"+inputCSVpath+"'");

			

			double[] nextLine = CSVReader.readArrayFromCSV(fileIn);
			double[] sensors = new double[ nextLine.length-3 ]; //-3, want reken eerste drie outputs (gas,remmen,sturen) niet mee.
			double[] normalisedInputs = new double[ nextLine.length-3 ]; //-3, want reken eerste drie outputs (gas,remmen,sturen) niet mee.


			csvOut.println("#"+ new SimpleDateFormat("dd MMM, yyyy HH:mm:ss").format(Calendar.getInstance().getTime()) ); //Code van: https:/stackoverflow.com/questions/5175728/how-to-get-the-current-date-time-in-java
			csvOut.print("#ACCELERATION,BRAKE,STEERING,SPEED,TRACK_POSITION,ANGLE_TO_TRACK_AXIS,"+
							"TRACK_EDGE_0,TRACK_EDGE_1,TRACK_EDGE_2,TRACK_EDGE_3,TRACK_EDGE_4,TRACK_EDGE_5,TRACK_EDGE_6,TRACK_EDGE_7,TRACK_EDGE_8,TRACK_EDGE_9,TRACK_EDGE_10,TRACK_EDGE_11,TRACK_EDGE_12,TRACK_EDGE_13,TRACK_EDGE_14,TRACK_EDGE_15,TRACK_EDGE_16,TRACK_EDGE_17,TRACK_EDGE_18");




			boolean extraSensors = false;
			//boolean opponentSensors = false;
			if(nextLine.length>=29 +3) { //Standaard 22 elementen, dus als 29, dan ook de extra sensoren uit zelfgemaakte set. +3, want ook nog snelheid, rem en stuur
				extraSensors = true;
				csvOut.print(",LATERAL_SPEED,Z_SPEED,Z,"+
							"WHEEL_SPIN_VELOCITY_0,WHEEL_SPIN_VELOCITY_1,WHEEL_SPIN_VELOCITY_2,WHEEL_SPIN_VELOCITY_3");
			}
			csvOut.print("\n");

			while(nextLine != null) {
				//Alle waarden vanaf kolom 3 zijn sensoren
				for(int i = 0; i < sensors.length; i++) {
					sensors[i] = nextLine[ i+3];
				}

				//Schrijf outputs naar nieuw bestand zonder aan te passen:
				csvOut.write(Double.toString(nextLine[0])+ //Snelheid is al genormaliseerd
							","+Double.toString(nextLine[1])+ //Rem is al genormaliseerd
							","+Double.toString(nextLine[2])); //Stuur is al genormaliseerd

				//Laat inputs normaliseren
				normaliseInputs(sensors, normalisedInputs);

				//Schrijf genormaliseerde waarden naar bestand
				for(int i = 0; i < normalisedInputs.length; i++) {
					csvOut.write(","+Double.toString(normalisedInputs[i]));
				}
				csvOut.write("\n");

				//Lees volgende regel
				nextLine = CSVReader.readArrayFromCSV(fileIn);
			}




			fileIn.close();
			csvOut.close();

			System.out.println("Written to file '"+outputCSVpath+"'");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param inputs			De array met daarin de inputs
	 * @param normalisedInputs	De array waarin de genormaliseerde inputs geschreven gaan worden. Er wordt gecontroleerd dat deze niet null is en dat de lengte van deze array gelijk is aan die van inputs.
	 */
	public static void normaliseInputs(double[] sensors, double[] normalisedInputs) {
		if(normalisedInputs == null) { throw new RuntimeException("Cannot normalise sensors, because one or both of the arrays have not been initialized."); }
		if(sensors.length != normalisedInputs.length) { throw new RuntimeException("Cannot normalise sensors, because the 'sensors' array has a different length than the 'normalisedInputs' array"); }



		normalisedInputs[SPEED]				= map( sensors[SPEED],	0.0, 300.0,	0.0, 1.0 ); //Snelheid: ga uit van range van 0 tot 300 (kmh)
		normalisedInputs[TRACK_POSITION]	= sensors[TRACK_POSITION]; 						//Trackposition: is al van -1 tot 1
		normalisedInputs[ANGLE_TO_TRACK_AXIS] = map( sensors[ANGLE_TO_TRACK_AXIS], -Math.PI, Math.PI, -1.0, 1.0 ); //Angle tot track as: van -Pi tot +Pi naar -1 tot +1


		for(int i = 0; i < 19; i++) { //19 TRACK_EDGE sensoren
			//Track edge wil nog wel eens meer dan 200 geven, maar is alleen onder 200 bruikbaar, dus contrain tot 0-200
			normalisedInputs[TRACK_EDGE+i] = map( constrain(sensors[TRACK_EDGE+i], -1.0, 200.0), -1.0, 200.0, 0.0, 1.0 );
		}



		if(sensors.length>=29) { //Als de extra sensoren er ook staan
			normalisedInputs[LATERAL_SPEED] = map(sensors[LATERAL_SPEED], 0.0, 300.0, 0.0, 1.0 );  //Ga uit van range van 0 tot 300 (kmh)
			normalisedInputs[Z_SPEED] = map(sensors[Z_SPEED], 0.0, 300.0, 0.0, 1.0 );  //Ga uit van range van 0 tot 300 (kmh)
			normalisedInputs[Z] = map(sensors[Z], 0.0, 10.0, 0.0, 1.0 ); //Ga uit van een afstand van 0 tot 10 meter (zwaartepunt van auto zal niet veel meer dan 10m van het oppervlak afkomen, hoop ik)
			for(int i = 0; i < 4; i++) {
				//Op 150 kmh is de wheelspin velocity 124 rad/s, dus ga uit van range van 0 tot 300 rad/s
				normalisedInputs[WHEEL_SPIN_VELOCITY+i] = map(sensors[WHEEL_SPIN_VELOCITY+i], 0.0, 300.0, 0.0, 1.0);
			}
		}
	}

	private static double constrain(double x, double minVal, double maxVal) {
		if( x < minVal ) {
			return minVal;
		}
		if( x > maxVal ) {
			return maxVal;
		}
		return x;
	}

	//Genomen van: https://www.arduino.cc/en/pmwiki.php?n=Reference/Map
	private static double map(double x, double fromLow, double fromHigh, double toLow, double toHigh) {
		return (x - fromLow) * (toHigh - toLow) / (fromHigh - fromLow) + toLow;
	}
}