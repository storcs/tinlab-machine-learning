package ann.util;


import java.time.LocalTime;

/**
 * Klasse om netwerken sequentieel te trainen.
 */
public class NetworkTrainer extends AbstractTrainer {
	private int sessions = 0;
	private int nanCount = 0;
	public static void main(String[] args) {

		LocalTime startTime = LocalTime.now();

/*
		String[] files = {	"alpine_2laps_total4.18_best2.06_0dmg.csv",
							"alpine_2laps_total4.21_best2.07_212dmg.csv",
							"alpine_2laps_total4.25_best2.09_231dmg.csv",
							"alpine_2laps_total4.30_best2.11-47dmg.csv" };
*/
/*
		String[] files = {	"CG_Speedway_1_3laps_total2.08_best0.40.csv",
							"CG_track_2_2laps_total1.55_best0.54.csv",
							"CG_track_2_3laps_total2.46_best0.52.csv",
							"CG_track_3_3laps_total3.17_best1.03.csv" };


		String directory = "configuration/size-test/ABSTRACT_1-hidden-layer_25-neurons_sigmoid_xavier-init/";
*/
		/*
		//String directory = "configuration/size-test/2-hidden-layers_62-tanh_62-tanh_linear-output_xavier-init/";
		//String directory = "configuration/size-test/1-hidden-layers_62-tanh_linear-output_xavier-init/";
		String directory = "configuration/size-test/3-hidden-layers_31-tanh_31-tanh_31-tanh_linear-output_xavier-init/";
		//String directory = "configuration/size-test/2-hidden-layers_27-tanh_25-tanh_xavier-init/";
		String csvDirectory = "../train_data/normalised/";
		String[] files = {	"aalborg_2laps_total2.35_best1.14.csv",
							"alpine_2laps_total4.18_best2.06_0dmg.csv",
							"CG_Speedway_1_3laps_total2.08_best0.40.csv",
							"CG_track_2_3laps_total2.46_best0.52.csv",
							"CG_track_3_3laps_total3.17_best1.03.csv",
							"Ruudskogen_total2.17_best1.06.csv",
							"Street_1_total2.47_best1.20.csv",
							"Brondehach_3laps_total4.09_best1.21.csv",
							"Wheel_1_3laps_total4.31_best1.28.csv",
							"Wheel_2_3laps_total5.47_best1.51.csv" };
							*/
/*
		String directory = "configuration/size-test/2-hidden_20-tanh_10-tanh_linear-output/";
		String csvDirectory = "../train_data/voorzichtiger/";
		String[] files = {	"Aalborg_3laps_4.18total_1.23best.csv",
							"Alpine-1_3laps_7.58total_2.38best.csv",
							"Alpine-2_3laps_5.33total_1.48best.csv",
							"Brondehach_3laps_4.42total_1.32best.csv",
							"CG-Speedway-number-1_3laps_2.15total_0.42best.csv",
							"CG-track-2_3laps_3.00total_0.57best.csv",
							"Corkscrew_3laps_4.45total_1.31best.csv",
							"E-Road_3laps_4.03total_1.18best.csv",
							"E-Track-6_3laps_4.59total_1.36best.csv",
							"Forza_3laps_5.03total_1.37best.csv",
							"Ruudskogen_3laps_3.44total_1.12best.csv",
							"Spring_1lap_8.42total_8.42best.csv",
							"Street-1_3laps_4.45total_1.32best.csv",
							"Wheel-1_3laps_4.57total_1.35best.csv",
							"Wheel-2_3laps_6.26total_2.05best.csv" };
*/

		//String directory = "configuration/elvira-dataset-test/2-hidden_60-tanh_60-tanh_tanh+linear-output/";
		//String directory = "configuration/elvira-dataset-test/2-hidden_30-tanh_20-tanh_tanh+linear-output/";
		String directory = "configuration/elvira-dataset-test/2-hidden_20-tanh_10-tanh_tanh+linear-output/";
		//String directory = "configuration/elvira-dataset-test/1-hidden_62-tanh_tanh+linear-output/";
		//String directory = "configuration/elvira-dataset-test/1-hidden_20-tanh_tanh+linear-output/";

		String csvDirectory = "../train_data/normalised/";
		String[] files = { //"aalborg.csv",
							//"suzuka.csv",
							//"Corners.csv",
							//"Corners_2.csv",
							//"Corners_3.csv",
							//"Corners_4.csv",
							//"Corners_version_2.csv",
							"Corners_version_2_1.csv",
							//"sprint.csv",
							//"alpine-1.csv",
							//"combined_data.csv",
							"combined_data.csv",
							"combined_data.csv" };
		/*
		_______________________________________________________
		Alpine sets:
				alpine_2laps_total4.18_best2.06_0dmg.csv
				alpine_2laps_total4.21_best2.07_212dmg.csv
				alpine_2laps_total4.25_best2.09_231dmg.csv
				alpine_2laps_total4.30_best2.11-47dmg.csv
		_______________________________________________________
		CG track sets:
				CG_Speedway_1_3laps_total2.08_best0.40.csv
				CG_track_2_2laps_total1.55_best0.54.csv
				CG_track_2_3laps_total2.46_best0.52.csv
				CG_track_3_3laps_total3.17_best1.03.csv
		_______________________________________________________
		
		"elvira-tracks":
				aalborg.csv
				suzuka.csv
				Corners.csv
				Corners_2.csv
				Corners_3.csv
				Corners_4.csv
				Corners_version_2.csv
				Corners_version_2_1.csv
				sprint.csv
				alpine-1.csv
				combined_data.csv
		_______________________________________________________
		elvira-combined:
				combined_data.csv
		_______________________________________________________
		elvira-various:
				3x combined_data.csv
				aalborg.csv
				alpine-1.csv
		_______________________________________________________
		additional-corners
				2x combined_data.csv
				Corners_version_2_1.csv
		_______________________________________________________
		*/
		
		//===============
		NetworkTrainer trainer;


/*
		trainer = new NetworkTrainer(	directory+"untrained/", //Begin configuratie
					directory+"trained-elvira-various_rate-0.01-to-error-1.0E-1/", //Eind configuratie
					csvDirectory, //CSV locatie
					files, //Array met CSVs
					100, //Hoeveel keer door alle bestanden
					0.01, //Learning rate
					1.0E-1, //Target error
					true); //Of gas en remmen tot één as gecombineerd moet worden
		trainer.trainSession();

		trainer = new NetworkTrainer(	directory+"trained-elvira-various_rate-0.01-to-error-1.0E-1/", //Begin configuratie
					directory+"trained-elvira-various_rate-0.0025-to-error-5.0E-2/", //Eind configuratie
					csvDirectory, //CSV locatie
					files, //Array met CSVs
					100, //Hoeveel keer door alle bestanden
					0.0025, //Learning rate
					5.0E-2, //Target error
					true); //Of gas en remmen tot één as gecombineerd moet worden
		trainer.trainSession();
*/
		trainer = new NetworkTrainer(	directory+"trained-elvira-various_rate-0.0001-to-error-1.0E-2_temp/", //Begin configuratie
					directory+"trained-elvira-various+10x-additional-corners@0.0001rate/", //Eind configuratie
					csvDirectory, //CSV locatie
					files, //Array met CSVs
					10, //Hoeveel keer door alle bestanden
					0.0001, //Learning rate
					0, //Target error
					true); //Of gas en remmen tot één as gecombineerd moet worden
		trainer.trainSession();




		LocalTime finishTime = LocalTime.now();
		System.out.println("Total start time: " + startTime);
		System.out.println("Total finish time: " + finishTime);
	}

	public NetworkTrainer(	String loadDirectory,
							String saveDirectory,
							String datasetDirectory,
							String[] datasetFilenames,
							int nrOfIterations,
							double learningRate,
							double targetError) {
		super(	loadDirectory,
				saveDirectory,
				datasetDirectory,
				datasetFilenames,
				nrOfIterations,
				learningRate,
				targetError);
	}

	public NetworkTrainer(	String loadDirectory,
							String saveDirectory,
							String datasetDirectory,
							String[] datasetFilenames,
							int nrOfIterations,
							double learningRate,
							double targetError,
							boolean combineThrottleAndBrake) { 
		super(	loadDirectory,
				saveDirectory,
				datasetDirectory,
				datasetFilenames,
				nrOfIterations,
				learningRate,
				targetError,
				combineThrottleAndBrake);
	}

}