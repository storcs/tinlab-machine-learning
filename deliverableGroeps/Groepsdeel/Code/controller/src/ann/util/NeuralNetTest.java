package ann.util;

import ann.network.NeuralNetwork;
import ann.util.io.CSVReader;



/**
 * Klasse om functionaliteiten van neuraal netwerk uit te proberen.
 * Ondertussen weinig nut meer.
 */
public class NeuralNetTest {
	public static void main(String[] args) {
		NeuralNetwork net;
		double[] inputs;
		double[] outputs;

		double targetVal;
		int iterations;
		double learningRate;

		//========== Test netwerk 4 ==========
		if(false) {
			net = NeuralNetworkBuilder.loadNetwork("configuration/testnetwork4/", "test-1.txt");
			net.printSize();
			//net.printBiases();
			net.printLayers();

			inputs = new double[]{1.0, 5.0, 9.0};
			outputs = net.getOutputs( inputs );

			System.out.println("\nVerwachtte outputs:\t0.4886\t0.5114"); //Bron: https://visualstudiomagazine.com/Articles/2014/11/01/Use-Python-with-Your-Neural-Networks.aspx

			System.out.print("Daadwerkelijke outputs:");
			for (int i = 0; i < outputs.length; i++) {
				System.out.print("\t"+outputs[i]);
			}
			System.out.println("\n");
		}



		//========== Test netwerk 3 ==========
		if(false) {
			net = NeuralNetworkBuilder.loadNetwork("configuration/testnetwork3/", "test.txt");
			net.printSize();
			//net.printBiases();
			net.printLayers();

			inputs = new double[]{0.3, 1.0};
			outputs = net.getOutputs( inputs );

			System.out.println("\nVerwachtte output:\t0.8"); //Bron: https://mlfromscratch.com/neural-networks-explained/

			System.out.print("Daadwerkelijke outputs:");
			for (int i = 0; i < outputs.length; i++) {
				System.out.print("\t"+outputs[i]);
			}
			System.out.println("\n");
		}


		//========== Test training van netwerk 3 ==========
		if(false) {
			net = NeuralNetworkBuilder.loadNetwork("configuration/testtrainingnetwork2/", "test.txt");
			net.printSize();
			//net.printBiases();
			net.printLayers();

			inputs = new double[]{0.3, 1.0};
			outputs = net.getOutputs( inputs );
			System.out.print("Standaard output:");
			for (int j = 0; j < outputs.length; j++) {
				System.out.print("\t"+outputs[j]);
			}
			System.out.println();

			targetVal = 0.8; //Tussen -1.0 en +1.0
			iterations = 1000;
			learningRate = 0.01;

			inputs = new double[]{0.3, 1.0};
			for(int i = 0; i < iterations; i++) {
				outputs = net.getOutputs( inputs ); //Vraag outputs op

				net.updateWeights(outputs, new double[]{targetVal}, learningRate); //Laat netwerk leren
			}
			

			System.out.print("\nGetrainde output:");
			for (int i = 0; i < outputs.length; i++) {
				System.out.print("\t"+outputs[i]);
			}
			System.out.println();

			System.out.println("Target output:\t"+targetVal);

			System.out.println("\nResulting network:");
			net.printSize();
			//net.printBiases();
			net.printLayers();
		}

		//========== Test training van netwerk 3 ==========
		if(false) {
			net = NeuralNetworkBuilder.loadNetwork("configuration/testtrainingnetwork3/untrained/", "test.txt");
			net.printSize();
			//net.printBiases();
			net.printLayers();

			inputs = new double[]{0.3, 1.0};
			outputs = net.getOutputs( inputs );
			System.out.print("Standaard output:");
			for (int j = 0; j < outputs.length; j++) {
				System.out.print("\t"+outputs[j]);
			}
			System.out.println();

			double[] targetValues = {0.4, -0.2}; //Tussen -1.0 en +1.0
			iterations = 1000;
			learningRate = 0.01;

			inputs = new double[]{0.3, 1.0};
			for(int i = 0; i < iterations; i++) {
				outputs = net.getOutputs( inputs ); //Vraag outputs op

				net.updateWeights(outputs, targetValues, learningRate); //Laat netwerk leren
			}
			

			System.out.print("\nGetrainde output:");
			for (int i = 0; i < outputs.length; i++) {
				System.out.print("\t"+outputs[i]);
			}
			System.out.println();
			System.out.println("Target output:\t"+targetValues[0]+",\t"+targetValues[1]);
			System.out.println("\nResulting network:");
			net.printSize();
			//net.printBiases();
			net.printLayers();

			//Probeer het huidige netwerk op te slaan:
			try {
				NetworkSaver.saveNetwork(net, "configuration/testtrainingnetwork3/trained/");
			} catch(Exception e) {
				e.printStackTrace();
			}

			//Open het zojuist opgeslagen netwerk
			net = NeuralNetworkBuilder.loadNetwork("configuration/testtrainingnetwork3/trained/", "files.txt");

			net.printLayers();
			//Train opnieuw, maar naar nieuwe waarden
			targetValues = new double[]{-0.7, 0.1}; //Tussen -1.0 en +1.0
			iterations = 1000;
			learningRate = 0.01;

			inputs = new double[]{-0.2, 0.8};
			System.out.println("Training network again...");
			for(int i = 0; i < iterations; i++) {
				outputs = net.getOutputs( inputs ); //Vraag outputs op

				net.updateWeights(outputs, targetValues, learningRate); //Laat netwerk leren
			}
			

			System.out.print("\nGetrainde output:");
			for (int i = 0; i < outputs.length; i++) {
				System.out.print("\t"+outputs[i]);
			}
			System.out.println();
			System.out.println("Target output:\t"+targetValues[0]+",\t"+targetValues[1]);
			System.out.println("\nResulting network:");
		}

		if(true) {
			net = NeuralNetworkBuilder.createNewNetwork(2);
			NeuralNetworkBuilder.addNewLayer(net,
									0, //Index (0-based)
									29, //Inputs
									10, //Aantal neuronen in deze laag
									CSVReader.TANH, //String met activation functie
									NeuralNetworkBuilder.XAVIER_INIT); //De manier van initialiseren van de gewichten

			NeuralNetworkBuilder.addNewLayer(net,
									1, //Index (0-based)
									10, //Inputs
									2, //Aantal neuronen in deze laag
									CSVReader.TANH, //String met activation functie
									NeuralNetworkBuilder.XAVIER_INIT); //De manier van initialiseren van de gewichten

			NeuralNetworkBuilder.setNeuronActivationFunction(net,
												1,	//Laag
												1,	//Neuron
												CSVReader.LINEAR); //Functie
			/*NeuralNetworkBuilder.setNeuronActivationFunction(net,
												1,	//Laag
												2,	//Neuron
												CSVReader.LINEAR); //Functie*/


			net.printLayers();
			try {
				NetworkSaver.saveNetwork(net, "configuration/size-test/1-hidden-layers_10-tanh_combined-tanh-output_xavier-init/untrained/");
			} catch(Exception e) {
				e.printStackTrace();
			}
		}

		if(false) {
			net = NeuralNetworkBuilder.loadNetwork("configuration/size-test/1-hidden-layers_10-tanh_combined-tanh-output_xavier-init/untrained/", "files.txt");
			net.printLayers();
			System.out.println("jaja");
		}
	}
}