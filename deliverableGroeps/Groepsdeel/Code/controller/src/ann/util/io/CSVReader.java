package ann.util.io;

import java.io.File;
import java.io.InputStream;
import java.util.Scanner;
import java.util.ArrayList;

import ann.math.function.ActivationFunction;
import ann.network.NeuronLayer;
import ann.util.NeuralNetworkBuilder;

public class CSVReader {
	public final static boolean useResourceStreams = true;
	public final static String DELIMITER = "\\s*\n\\s*",
								COMMENT = "#",
								LINEAR = "linear",
								SIGMOID = "sigmoid",
								TANH = "tanh",
								SOFTMAX = "softmax";

	/**
	 * Maak static, zodat aan elke neuronlaag hetzelfde SigmoidFunction object
	 * meegegeven kan worden. Op dit manier worden niet onnodig veel objecten
	 * aangemaakt. 
	 */
/*
	public static ActivationFunction linear = new Linear();
	public static ActivationFunction sigmoid = new Sigmoid();
	public static ActivationFunction tanh = new HyperbolicTangent();
	public static ActivationFunction softmax = new Softmax();
*/

	/**
	 * Leest een laag van het netwerk uit een csv bestand
	 * Returnt null als bestand niet geopend kon worden.
	 */
	public static NeuronLayer readNeuronLayerFromCSV(String filepath) {
		System.out.println("Reading neuron from '"+filepath+"'.");
		NeuronLayer layer = null;

		try {
			Scanner fileIn = null;
			if(useResourceStreams) {
				InputStream in = CSVReader.class.getClassLoader().getResourceAsStream(filepath);
				if(in == null) {
					throw new Exception("Could not open resource stream.");
				}
				fileIn = new Scanner(in).useDelimiter(CSVReader.DELIMITER);	
			} else {
				File file = new File(filepath);
				//Scanner met newlines als delimiter. Andere whitespace rond newline karakter wordt genegeerd.
				fileIn = new Scanner(file).useDelimiter(DELIMITER);
			}


			layer = new NeuronLayer();

			//========== Stel activation functie in ==========
			//Stop als f ingesteld is of als geen regels meer zijn om te lezen
			/*while(layer.f == null && fileIn.hasNext()) {

				String inputLine = fileIn.next(); //Lees de volgende regel

				if(!inputLine.startsWith(COMMENT)) {

					//Ga alle activation functies af;
					if(inputLine.toLowerCase().equals(LINEAR)) {
						layer.f = NeuralNetworkBuilder.getLinearInstance();
					} else if(inputLine.toLowerCase().equals(SIGMOID)) {
						layer.f = NeuralNetworkBuilder.getSigmoidInstance();
					} else if(inputLine.toLowerCase().equals(TANH)) {
						layer.f = NeuralNetworkBuilder.getHyperbolicTangentInstance();
					} else if(inputLine.toLowerCase().equals(SOFTMAX)) {
						layer.f = NeuralNetworkBuilder.getSoftmaxInstance();
					}

				}
			}*/
			while(layer.f == null && fileIn.hasNext()) {

				String inputLine = fileIn.next(); //Lees de volgende regel

				if(!inputLine.startsWith(COMMENT)) {

					String[] functions = inputLine.toLowerCase().split(",");

					layer.f = new ActivationFunction[ functions.length ];

					for(int i = 0; i < functions.length; i++) {
						
						//Ga alle activation functies af:
						if(functions[i].equals(LINEAR)) {
							layer.f[i] = NeuralNetworkBuilder.getLinearInstance();
						} else if(functions[i].equals(SIGMOID)) {
							layer.f[i] = NeuralNetworkBuilder.getSigmoidInstance();
						} else if(functions[i].equals(TANH)) {
							layer.f[i] = NeuralNetworkBuilder.getHyperbolicTangentInstance();
						} else if(functions[i].equals(SOFTMAX)) {
							layer.f[i] = NeuralNetworkBuilder.getSoftmaxInstance();
						} else {
							throw new RuntimeException("Unknown activation function: "+functions[i]);
						}
					}
				}
			}

			//========== lees biases ==========
			layer.weight = readMatrixFromCSV( fileIn );

			//========== Stel filepath in ==========
			layer.path = filepath;

			fileIn.close();
		} catch(Exception e) {
			e.printStackTrace();
		}

		return layer;
	}


	/**
	 * Leest een matrix uit een csv bestand. De matrix is een array van rijen, dus
	 * elke regel uit de csv wordt een rij, waarbij de matrix een array van rijen is.<br>
	 * Maakt gebruik van ArrayList van arrays en converteert dit terug naar een
	 * array van arrays. Denk dat gebruik in hot-loops geen goed idee is (alhoewel file IO dat sowieso al niet is).<br>
	 * <br>
	 * Negeert regels die beginnen met CSVReader#COMMENT
	 * @param fileIn	Een scanner met een CSV bestand. De scanner moet al geopend zijn en wordt in deze functie niet gesloten.
	 */
	public static double[][] readMatrixFromCSV(Scanner fileIn) {
		ArrayList<double[]> rowList = new ArrayList<double[]>();

		while(fileIn.hasNext()) {
			double[] row = readArrayFromCSV(fileIn);
			if(row != null) {
				rowList.add( row );
			}
		}

		//Converteer naar arrayvorm en return
		return rowList.toArray( new double[rowList.size()][] );
	}

	/**
	 * Leest een array uit een csv. Gebruikt de eerste regel die geen comment is. Comments
	 * moeten op hun eigen regel staan en beginnen met CSVReader#COMMENT
	 * Returnt null als geen regels gevonden worden die geen comment zijn.
	 * @param fileIn	Een scanner met een CSV bestand. De scanner moet al geopend zijn en wordt in deze functie niet gesloten.
	 */
	public static double[] readArrayFromCSV(Scanner fileIn) {
		double[] arr = null;

		String inputLine = "";
		do {
			if(fileIn.hasNext()) {
				
				inputLine = fileIn.next();

				if(!inputLine.startsWith(COMMENT)) {

					String[] values =  inputLine.split(","); //Een array van de strings tussen de commas

					arr = new double[values.length]; //instantieer de array, omdat nu pas bekend is hoe groot hij moet worden

					for(int i = 0; i < values.length; i++) {
						arr[i] = Double.parseDouble(values[i]);
					}
				}
			}
		} while (fileIn.hasNext() && arr == null);

		return arr;
	}
}