package stonks;

import java.lang.Math;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import stonks.input.CustomController;
import stonks.input.CustomKeyboard;
import stonks.input.Joystick;
import ann.util.DataNormaliser;

/**
 * Een Driver waarmee zelf datasets gemaakt kunnen worden. Maakt datasets die 7
 * sensoren meer gebruiken dan die van Elvira, dus mixen kan waarschijnlijk niet.
 */
public class DatasetCreator extends AbstractStonksDriver{
	private CustomController controller; //Het controller object voor de joystick of het toetsenbord
	private final String outputFilePath = "../train_data/CG-Speedway-number-1.csv"; //Het bestand waarnaar de dataset geschreven gaat worden.
	private File file; //Bestand dat gebruikt wordt voor de output.
	private java.io.PrintWriter csvOut; //De writer die gebruikt wordt om naar DatasetCreator#File te schrijven.

	private boolean addOpponents = false; //Als true, dan worden ook opponentsensoren naar dataset geschreven.

	private double[] inputs = new double[29]; //Array voor inputs
	private double[] normalisedInputs = new double[29]; //Array voor genormaliseerde inputs.

	public DatasetCreator() {
		super(); //Constructor van base klasse voor de zekerheid
		controller = new Joystick(); //Probeer eerst joystick te vinden.
		if(!controller.initializeGamepad()) { //Als niet gevonden, dan:
			System.out.println("Could not find gamepad, looking for keyboard.");
			controller = new CustomKeyboard(); //Gebruik toetsenbord.
			if(controller.initializeGamepad()) {
				System.out.println("Found keyboard.");
			} else {
				System.out.println("Could not find keyboard, exiting...");
			}
			
		} else {
			System.out.println("Found gamepad.");
		}

		try {
			file = new File(outputFilePath);
			csvOut = new java.io.PrintWriter(file); //Open het bestand
			csvOut.println("#"+ new SimpleDateFormat("dd MMM, yyyy HH:mm:ss").format(Calendar.getInstance().getTime()) ); //Code van: https:/stackoverflow.com/questions/5175728/how-to-get-the-current-date-time-in-java
			csvOut.print("#ACCELERATION,BRAKE,STEERING,SPEED,TRACK_POSITION,ANGLE_TO_TRACK_AXIS,"+
							"TRACK_EDGE_0,TRACK_EDGE_1,TRACK_EDGE_2,TRACK_EDGE_3,TRACK_EDGE_4,TRACK_EDGE_5,TRACK_EDGE_6,TRACK_EDGE_7,TRACK_EDGE_8,TRACK_EDGE_9,TRACK_EDGE_10,TRACK_EDGE_11,TRACK_EDGE_12,TRACK_EDGE_13,TRACK_EDGE_14,TRACK_EDGE_15,TRACK_EDGE_16,TRACK_EDGE_17,TRACK_EDGE_18,"+
							"LATERAL_SPEED,Z_SPEED,Z,"+
							"WHEEL_SPIN_VELOCITY_0,WHEEL_SPIN_VELOCITY_1,WHEEL_SPIN_VELOCITY_2,WHEEL_SPIN_VELOCITY_3");
			if(addOpponents) {
				csvOut.print(",OPPONENT_0,OPPONENT_1,OPPONENT_2,OPPONENT_3,OPPONENT_4,OPPONENT_5,OPPONENT_6,OPPONENT_7,OPPONENT_8,OPPONENT_9,"+
								"OPPONENT_10,OPPONENT_11,OPPONENT_12,OPPONENT_13,OPPONENT_14,OPPONENT_15,OPPONENT_16,OPPONENT_17,OPPONENT_18,OPPONENT_19,"+
								"OPPONENT_20,OPPONENT_21,OPPONENT_22,OPPONENT_23,OPPONENT_24,OPPONENT_25,OPPONENT_26,OPPONENT_27,OPPONENT_28,OPPONENT_29,"+
								"OPPONENT_30,OPPONENT_31,OPPONENT_32,OPPONENT_33,OPPONENT_34,OPPONENT_35");
			}
			csvOut.print("\n");
				
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
			
	}

	@Override
	public Action control(SensorModel sensors) {
		Action action = new Action();

		readJoystickValues(action); //Input van gebruiker inlezen

		//========== Uitlezen ==========
		//Zet alle waarden in de input array om te laten normaliseren 

		//Lees sensoren en zet waarden in input array
		sensorsToInputs(sensors, inputs);

		if(addOpponents) { //Nog niet geimplementeerd...
			throw new RuntimeException("Not adding opponents to inputs, because DataNormaliser does not have any code for it.");
		}


		//========== Normaliseren ==========
		DataNormaliser.normaliseInputs(inputs, normalisedInputs); //Normaliseer alle inputs


		//========== Naar CSV schrijven ==========
		//Gas, remmen en sturen is al tussen -1 en 1, dus hoeft niets aan genormaliseerd.
		csvOut.write(Double.toString(action.accelerate)+"," + Double.toString(action.brake)+"," + Double.toString(action.steering)+",");

		//Schrijf alle inputs naar de csv:
		for(int i = 0; i < normalisedInputs.length; i++) {
			csvOut.write(Double.toString(normalisedInputs[i]));

			if(i!=normalisedInputs.length-1) {
				csvOut.write(",");
			}
		}
		csvOut.write("\n");

		//Printen voor gebruiksgemak:
		System.out.println(	"Accel:\t" + action.accelerate+
							"\tBrake:\t" + action.brake+
							"\tSteering:\t" + action.steering);


		//Schakelen en abs automatisch, want dat hoeft het netwerk niet zelf te doen:
		action.gear = calculateGear(sensors);
		applyClutch(action);
		action.brake = applyABS(sensors, (float)action.brake);

		return action;
	}



	@Override
	public void reset() {

	}

	@Override
	public void shutdown() {
		csvOut.close(); //Writer sluiten
		controller.close(); //Geef controle van joystick of toetsenbord weer op.
	}

	/**
	 * Leest de waarden van de joystick en schrijft ze naar de action. Doet alleen
	 * action.accelerate, action.brake en action.steering. Andere waarden blijven
	 * onaangetast.
	 * @param action 	De Action waarnaartoe de joystick positie geschreven wordt.
	 */
	private void readJoystickValues(Action action) {
		controller.poll();
		action.accelerate = controller.getAccelerate();
		action.brake = controller.getBrake();
		action.steering = controller.getSteering();
	}

}
