package stonks;

/**
 * Deze loader is het entry point van het .jar bestand.
 * Stelt stonks.StonksDriver in, maar andere argumenten
 * worden doorgegeven aan stonks.Client.
 */
public class Loader {

	public static void main(String[] args) {
		String[] newArgs = new String[args.length+1];

		newArgs[0] = "stonks.StonksDriver"; //Stel StonksDriver als default in

		for(int i = 0;i<args.length;i++) { //Pak andere argumenten ook mee
			newArgs[i+1] = args[i];
		}

		Client.main(newArgs); //Laad alsnog main met nieuwe argumenten
	}

}
