package stonks;
/* recovery driver to put the car back on track
recovery driver is based of:
http://sourceforge.net/projects/cig/
the simpledriver from torcs

*/
public class RecoveryDriver extends Controller{
	private double steerLock=(float) 0.366519;
public void recovery() {
	
	
	
}


@Override
public Action control(SensorModel sensors) {
	/*
	 * set gear and sterring command assuming car is pointing in a direction out of
	 * track
	 */
	Action action = new Action();
	// to bring car parallel to track axis
	float steer = (float) (-sensors.getAngleToTrackAxis() / steerLock);
	int gear = -1; // gear R

	// if car is pointing in the correct direction revert gear and steer
	if (sensors.getAngleToTrackAxis() * sensors.getTrackPosition() > 0) {
		gear = 1;
		steer = -steer;
	}

	// build a CarControl variable and return it

	action.gear = gear;
	action.steering = steer;
	action.accelerate = 1;
	action.brake = 0;

	return action;
}

@Override
public void reset() {
	// TODO Auto-generated method stub
	
}

@Override
public void shutdown() {
	// TODO Auto-generated method stub
	
}	
	
}