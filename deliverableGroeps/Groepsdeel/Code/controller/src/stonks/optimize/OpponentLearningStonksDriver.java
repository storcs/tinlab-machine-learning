package stonks.optimize;

import stonks.*;

public class OpponentLearningStonksDriver extends StonksDriver {
	private final int nrOfLaps = 1; //Het aantal ronden waarover een gemiddelde genomen moet worden
	private final double maxLaptime = 240.0; //Als de huidig laptim groter is dan deze waarde, wordt de sessie gestopt en wordt een nieuwe configuratie gegenereerd
	private final String metaCSVLocation = "configuration/final/TESTmeta.csv";
	private final String opponentCSVLocation = "configuration/final/TESTopponentInfluence.csv";

	private static java.util.Random r = new java.util.Random();

	private int lapsCompleted = 0;
	private double lastLaptime = 0.0;
	private double averageLaptime = 0.0;
	private double avgDamage = 0.0;
	private int nrOfResets = 0;
	private boolean warmupLap = true;


	public OpponentLearningStonksDriver() {
		super();

		//reset();
		

		opponentInfluence = getRandomInfluence();
		/*System.out.println("Influence:");
		for(int x = 0; x < opponentInfluence.length; x++) {
			for (int y = 0; y < opponentInfluence[x].length; y++) {
				for (int z = 0; z < opponentInfluence[x][y].length; z++) {
					System.out.print(" "+opponentInfluence[x][y][z]+",");
				}
				System.out.print(" | ");
			}
			System.out.println();
		}*/


	}

	@Override
	public Action control(SensorModel sensors) {
		Action action = super.control(sensors);

		if(lastLaptime != sensors.getLastLapTime() ) {
			if(!warmupLap) {
				System.out.println("Lap completed.");
				lastLaptime = sensors.getLastLapTime();
				averageLaptime = (averageLaptime*lapsCompleted + lastLaptime) / (lapsCompleted+1);
				lapsCompleted++;

				if (lapsCompleted >= nrOfLaps) {
					overwriteIfNeeded(averageLaptime, avgDamage / (double)nrOfLaps);

					if(sensors.getDamage() > 10.0) {
						System.out.println("Too much damage, requesting restart...");
						requestRestart(action, 1000, 100000); //Deze instelling niet goed, dus geef onzinnige waarden aan requestRestart die nooit beter zijn dan huidige
					} else {
						System.out.println("Enough laps completed, resetting...");
						reset();
					}
				}
			} else {
				System.out.println("Completed warmup lap.");
				warmupLap = false;
				lastLaptime = sensors.getLastLapTime();
			}
				
		}
		if(sensors.getCurrentLapTime() > maxLaptime) { //Als ronde meer dan 1 minuten duurt
			
			System.out.println("Too long to complete lap, requesting restart...");
			requestRestart(action, 1000, 100000); //Deze instelling niet goed, dus geef onzinnige waarden aan requestRestart die nooit beter zijn dan huidige
		}
		if(sensors.getFuelLevel() < 10.0) {
			System.out.println("No more fuel, requesting restart...");
			requestRestart(action, 1000, 100000); //Deze instelling niet goed, dus geef onzinnige waarden aan requestRestart die nooit beter zijn dan huidige
		}



		return action;
	}

	@Override
	public void reset() {
		super.reset();

		// TODO Auto-generated method stub
		nrOfResets++;
		System.out.println("\nReset(" + nrOfResets + ")\n");

		lapsCompleted = 0;
		//lastLaptime = 0.0;
		averageLaptime = 0.0;
		avgDamage = 0.0;
		warmupLap = true;

		opponentInfluence = getSuccessor(opponentInfluence);
	
	}

	@Override
	public void shutdown() {
		super.shutdown();
	}



	//============== Search =========================
	private void requestRestart(Action action, double averageLaptime, double avgDamage) {
		overwriteIfNeeded(averageLaptime, avgDamage);
		action.restartRace = true;
	}

	private void overwriteIfNeeded(double averageLaptime, double avgDamage) {
		double previousBestLaptime = 0.0;
		double previousLowestDamage = 0.0;
		double[] meta = getMetaFromCSV(metaCSVLocation);
		previousBestLaptime = meta[TIME];
		previousLowestDamage = meta[DAMAGE];


		System.out.println("Current setup average time: " + averageLaptime +" ("+ avgDamage +" dmg). Best setup average time: " + previousBestLaptime +" ("+ previousLowestDamage +" dmg).");

		if(previousBestLaptime > averageLaptime/* && previousLowestDamage >= avgDamage+1.0*/) {	//Plus 1.0 omdat anders problemen wanneer beide 0.0 of bijna 0.0 zijn...
			System.out.println("Better configuration found. Writing to csv...");
			writeMetaToCSV(averageLaptime, avgDamage, metaCSVLocation);
			writeToCSV(opponentInfluence, opponentCSVLocation);
		} else {
			System.out.println("Configuration not better, trying another.");
		}
	}

	private static double[][][] getSuccessor(double[][][] influence) {

		for(int i = 0; i < 18; i++) {
			influence[i][STEERING][SPRINGRATE] +=	0.5 * r.nextGaussian();
			influence[i][STEERING][DAMPING] +=		1.0 * r.nextGaussian();
			influence[i][STEERING][THRESHOLD] +=	10.0 * r.nextGaussian();
			influence[i][STEERING][VFACTOR] +=		1.0 * r.nextGaussian();

			influence[i][SPEED][SPRINGRATE] +=		0.5 * r.nextGaussian();
			influence[i][SPEED][DAMPING] +=			1.0 * r.nextGaussian();
			influence[i][SPEED][THRESHOLD] +=		50.0 * r.nextGaussian();
			influence[i][SPEED][VFACTOR] +=			1.0 * r.nextGaussian();

			//Houd symmetrisch:
			influence[influence.length-(1+i)][STEERING][SPRINGRATE]	= -influence[i][STEERING][SPRINGRATE];
			influence[influence.length-(1+i)][STEERING][DAMPING]	= -influence[i][STEERING][DAMPING];
			influence[influence.length-(1+i)][STEERING][THRESHOLD]	= -influence[i][STEERING][THRESHOLD];
			influence[influence.length-(1+i)][STEERING][VFACTOR]	= -influence[i][STEERING][VFACTOR];
			influence[influence.length-(1+i)][SPEED][SPRINGRATE]	= influence[i][SPEED][SPRINGRATE];
			influence[influence.length-(1+i)][SPEED][DAMPING]		= influence[i][SPEED][DAMPING];
			influence[influence.length-(1+i)][SPEED][THRESHOLD]		= influence[i][SPEED][THRESHOLD];
			influence[influence.length-(1+i)][SPEED][VFACTOR]		= influence[i][SPEED][VFACTOR];
		}

		return influence;
	}

	private static double[][][] getRandomInfluence() {
		double[][][] retArr = new double[36][2][5];
/*

		for(int i = 0; i < 18; i++) {
			retArr[i][STEERING][SPRINGRATE] += 	0.5 * r.nextGaussian();		//getRandomDouble(	-0.5,	0.5);
			retArr[i][STEERING][DAMPING] += 	1.0 * r.nextGaussian();		//getRandomDouble(	-1.0,	1.0);
			retArr[i][STEERING][THRESHOLD] += 	10.0 * r.nextGaussian();	//getRandomDouble(	1.0,	10.0);
			retArr[i][STEERING][VFACTOR] += 	1.0 * r.nextGaussian();		//getRandomDouble(	0.0, 	1.0);

			retArr[i][SPEED][SPRINGRATE] += 	0.5 * r.nextGaussian();		//getRandomDouble(	0.5,	0.5);
			retArr[i][SPEED][DAMPING] += 		1.0 * r.nextGaussian();		//getRandomDouble(		-1.0, 	1.0);
			retArr[i][SPEED][THRESHOLD] += 		50.0 * r.nextGaussian();	//getRandomDouble(		1.0,	50.0);
			retArr[i][SPEED][VFACTOR] += 		1.0 * r.nextGaussian();		//getRandomDouble(		0.0, 	1.0);

			//Houd symmetrisch:
			retArr[retArr.length-(1+i)][STEERING][SPRINGRATE]	= retArr[i][STEERING][SPRINGRATE];
			retArr[retArr.length-(1+i)][STEERING][DAMPING]		= retArr[i][STEERING][DAMPING];
			retArr[retArr.length-(1+i)][STEERING][THRESHOLD]	= retArr[i][STEERING][THRESHOLD];
			retArr[retArr.length-(1+i)][STEERING][VFACTOR]		= retArr[i][STEERING][VFACTOR];
			retArr[retArr.length-(1+i)][SPEED][SPRINGRATE]		= retArr[i][SPEED][SPRINGRATE];
			retArr[retArr.length-(1+i)][SPEED][DAMPING]			= retArr[i][SPEED][DAMPING];
			retArr[retArr.length-(1+i)][SPEED][THRESHOLD]		= retArr[i][SPEED][THRESHOLD];
			retArr[retArr.length-(1+i)][SPEED][VFACTOR]			= retArr[i][SPEED][VFACTOR];
		}

*/
		return retArr;
	}

	private static int getRandomInt(int from, int to) { //Random int tussen from en to (inclusief from en to)
		if( from > to) {
			return getRandomInt(to, from);
		}

		int delta = to - from;

		//Delta+1, want nextInt is exlusief
		return from + r.nextInt(delta+1);

	}

	//Gediscretiseerd om te voorkomen dat te kleine waarden gebruikt worden neemt 20 stappen tussen from en to
	private static double getRandomDouble(double from, double to) { //Random int tussen from en to (inclusief from en to)
		if( from > to) {
			return getRandomDouble(to, from);
		}

		int steps = 20;
		double slice = (to - from)/steps;

		return from + slice*getRandomInt(0, steps);
	}


	//==============================
	private static void writeMetaToCSV(double averageLaptime, double avgDamage, String filename) {
		try {
			java.io.File csvFile = new java.io.File(filename);
	    	java.io.PrintWriter fileOut = new java.io.PrintWriter(csvFile);


			fileOut.write( Double.toString(averageLaptime) + "\n" );
			fileOut.write( Double.toString(avgDamage) + "\n" );

			fileOut.close();

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	private static int TIME = 0, DAMAGE = 1;
	private static double[] getMetaFromCSV(String filename) {
		double averageLaptime = -1.0;
		double avgDamage = -1.0;
		try {
			java.io.File csvFile = new java.io.File(filename);
			java.util.Scanner fileIn = new java.util.Scanner(csvFile);

			averageLaptime = Double.parseDouble(fileIn.next());
			avgDamage = Double.parseDouble(fileIn.next());

			fileIn.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new double[] {averageLaptime, avgDamage};
	}

	private static void writeToCSV(double[][][] array, String filename) {
		//trackEdgeInfluence

		try {

			java.io.File csvFile = new java.io.File(filename);
	    	java.io.PrintWriter fileOut = new java.io.PrintWriter(csvFile);

			for(int row = 0; row < array.length; row++ ) {	//Voor alle sensoren
				StringBuffer str = new StringBuffer();	//String voor elke regel

				for(int i = 0; i < 2; i++) {	//Voor zowel sturen als gasgeven
					for(int col = 0; col < (array[row][i]).length; col++ ) {	//Ga alle waarden af
						str.append( Double.toString(array[row][i][col]) + "," ); //Append met een comma
					}
				}

				str.deleteCharAt(str.length()-1); //Haal laatste comma weg
				str.append("\n");

				fileOut.write(str.toString());	//Schrijf weg naar bestand				
			}

			fileOut.close();

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}